//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.fbsgen.ds.map;

import static com.dyuproject.fbsgen.compiler.ErrorUtil.err;
import static com.dyuproject.fbsgen.compiler.ErrorUtil.getProto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import com.dyuproject.fbsgen.compiler.CodegenException;
import com.dyuproject.fbsgen.compiler.ErrorUtil;
import com.dyuproject.fbsgen.compiler.FakeMap;
import com.dyuproject.fbsgen.compiler.Writable;
import com.dyuproject.fbsgen.ds.Config;
import com.dyuproject.fbsgen.ds.Config.Value.Permutation;
import com.dyuproject.fbsgen.ds.EntityUtil;
import com.dyuproject.fbsgen.ds.QueryUtil;
import com.dyuproject.fbsgen.ds.Verbs;
import com.dyuproject.fbsgen.parser.Annotation;
import com.dyuproject.fbsgen.parser.CodegenUtil;
import com.dyuproject.fbsgen.parser.EnumField;
import com.dyuproject.fbsgen.parser.EnumGroup;
import com.dyuproject.fbsgen.parser.Field;
import com.dyuproject.fbsgen.parser.Message;
import com.dyuproject.fbsgen.parser.MessageField;
import com.dyuproject.fbsgen.parser.Proto;
import com.dyuproject.fbsgen.parser.Service.RpcMethod;

/**
 * A map that retrieves any value from any param.
 * 
 * @author David Yu
 * @created Apr 9, 2013
 */
public final class GetMap extends FakeMap
{
    
    public interface Function
    {
        Object get(Object data);
    }

    public final Function func;
    
    public GetMap(String name, Function func)
    {
        super(name);
        this.func = func;
    }
    
    public Object get(Object key)
    {
        return func.get(key);
    }
    
    public static void addAllTo(List<FakeMap> list)
    {
        for (Functions c : Functions.values())
            list.add(c.map);
    }
    
    public enum Functions implements Function
    {
        OP_PKG
        {
            public Object get(Object data)
            {
                Proto proto = (Proto)data;
                String opPkg = proto.getExtraOption("op_pkg");
                if (opPkg == null)
                    return proto.getJavaPackageName();
                
                if (opPkg.charAt(0) == '.')
                    return proto.getJavaPackageName() + opPkg;
                
                return opPkg;
            }
        },
        
        VIEW_PKG
        {
            public Object get(Object data)
            {
                Proto proto = (Proto)data;
                String viewPkg = proto.getExtraOption("view_pkg");
                if (viewPkg == null)
                    return proto.getJavaPackageName();
                
                if (viewPkg.charAt(0) == '.')
                    return proto.getJavaPackageName() + viewPkg;
                
                return viewPkg;
            }
        },
        
        
        VERB_VIEW
        {
            public Object get(Object data)
            {
                RpcMethod rm = (RpcMethod)data;
                String name = rm.getName();
                
                if (name.startsWith("get"))
                    return Verbs.View.GET;
                
                if (name.startsWith("list"))
                    return Verbs.View.LIST;
                
                if (name.startsWith("find"))
                    return Verbs.View.FIND;
                
                if (name.charAt(0) == 'q' && Verbs.isUppercase(name.charAt(1)))
                    return Verbs.View.Q;
                
                return null;
            }
        },
        
        VERB_OP
        {
            public Object get(Object data)
            {
                RpcMethod rm = (RpcMethod)data;
                String name = rm.getName();
                
                if (name.startsWith("new"))
                    return Verbs.Op.NEW;
                
                if (name.startsWith("update"))
                    return Verbs.Op.UPDATE;
                
                if (name.startsWith("delete"))
                    return Verbs.Op.DELETE;
                
                if (name.startsWith("mark"))
                    return Verbs.Op.MARK;
                
                return null;
            }
        },
        
        FIELD_TYPE
        {
            public Object get(Object data)
            {
                Field<?> f = (Field<?>)data;
                if (f.isBytesField())
                {
                    Message message;
                    Annotation key = f.getAnnotation("Key");
                    if (key == null || (message = key.getValue("entity")) == null)
                    {
                        throw err("The field: " + f.getName() + " of " + 
                                f.getOwner().getRelativeName() + 
                                " must be annotated with @Key(entity = SomeEntity)", 
                                f.getProto());
                    }
                    
                    return message;
                }
                
                if (!f.isMessageField())
                    return null;
                
                // TODO
                //Message message = ((MessageField)f).getMessage();
                
                
                return null;
            }
        },
        
        HASID_FIELD
        {
            public Object get(Object data)
            {
                MessageField f = (MessageField)data;
                Message m = f.getMessage();
                Annotation a = m.getAnnotation("HasId");
                String f0 = a.getValue("f0");
                
                return f0 != null ? m.getField(f0) : m.getField("id");
            }
        },
        
        HASKEY_FIELD
        {
            public Object get(Object data)
            {
                MessageField f = (MessageField)data;
                Message m = f.getMessage();
                Annotation a = m.getAnnotation("HasKey");
                String f0 = a.getValue("f0");
                
                return f0 != null ? m.getField(f0) : m.getField("key");
            }
        },
        
        HASPARENTKEY_FIELD
        {
            public Object get(Object data)
            {
                MessageField f = (MessageField)data;
                Message m = f.getMessage();
                Annotation a = m.getAnnotation("HasParentKey");
                String f0 = a.getValue("f0");
                
                return f0 != null ? m.getField(f0) : m.getField("parent_key");
            }
        },
        
        CONFIG
        {
            public Object get(Object data)
            {
                return data == null ? null : Config.getConfig((EnumGroup)data);
            }
        },
        
        ENTITY_FLAGS
        {
            public Object get(Object data)
            {
                Message entity = (Message)data;
                Annotation a = entity.getAnnotation("Entity");
                
                int flags = 0;
                
                if (Boolean.TRUE.equals(a.getP().get("seq")))
                    flags |= 1;
                
                return flags;
            }
        },
        
        ENTITY_PARENT
        {
            public Object get(Object data)
            {
                return EntityUtil.getEntityParent((Message)data);
            }
        },
        
        ENTITY_ROOT
        {
            public Object get(Object data)
            {
                return EntityUtil.getEntityRoot((Message)data);
            }
        },
        
        ENTITY_ROOT_IF_NESTED
        {
            public Object get(Object data)
            {
                return EntityUtil.getEntityRootIfNested((Message)data);
            }
        },
        
        ENTITY_RPC_RETURN_TYPE
        {
            public Object get(Object data)
            {
                RpcMethod rm = (RpcMethod)data;
                Message message = rm.getReturnType();
                
                if (IsMessageMap.Functions.ENTITY.query(message))
                    return message;
                
                if (message.getName().equals("PList"))
                {
                    Message parent = message.getParentMessage();
                    if (IsMessageMap.Functions.ENTITY.query(parent))
                        return parent;
                }
                
                return null;
            }
        },
        
        ENTITY_IDX_ID
        {
            public Object get(Object obj)
            {
                Config.Value v = (Config.Value)obj;
                final boolean tag = v.getNumber() < 224;
                
                final StringBuilder sb = new StringBuilder();
                int start = 0;
                if (tag)
                {
                    // tag idx
                    if (v.getEntries().isEmpty())
                    {
                        throw err("The tag (1-223) index entry: " + 
                                v.getEg().getRelativeName() + "." + v.getName() + 
                                " must have an option f0 that points to a tag.", v.getV());
                    }
                    
                    Config.Value.Entry entry = v.getEntries().get(start++);
                    EnumGroup.Value ev = ((EnumField)entry.getField()).getEv();
                    
                    sb.append(ev.getEg().getRelativeName())
                        .append('.')
                        .append(ev.getName());
                    
                    if (0 == (v.getEntrySize() - start))
                        return sb.toString();
                }
                else
                {
                    // entity idx
                    sb.append("IDX_").append(v.getName());
                    
                    if (v.getEntries().isEmpty())
                        return sb.toString();
                }
                
                sb.append(", 0");
                
                for (int i = start, len = v.getEntrySize(); i < len; i++)
                {
                    Config.Value.Entry entry = v.getEntries().get(i);
                    EnumGroup.Value ev = ((EnumField)entry.getField()).getEv();
                    
                    sb.append(", ")
                        .append(ev.getEg().getRelativeName())
                        .append('.')
                        .append(ev.getName());
                }
                
                return sb.append(", 0").toString();
            }
        },
        
        ENTITY_IDX_STRING
        {
            public Object get(Object obj)
            {
                if (obj instanceof EnumGroup.Value)
                {
                    EnumGroup.Value v = (EnumGroup.Value)obj;
                    
                    return QueryUtil.getUusvIdxString(v.getNumber() < 224, 
                            v, v.getEnumGroup().getParentMessage());
                }
                
                Config.Value v = (Config.Value)obj;
                
                return QueryUtil.getUusvIdxString(v.getNumber() < 224, 
                        v.getV(), v.getEnumGroup().getParentMessage());
            }
        },
        
        ENTITY_IDX_CSV
        {
            public Object get(Object obj)
            {
                if (obj instanceof EnumGroup.Value)
                {
                    EnumGroup.Value v = (EnumGroup.Value)obj;
                    
                    return QueryUtil.getUusvIdxCsv(v.getNumber() < 224, 
                            v, v.getEnumGroup().getParentMessage());
                }
                
                Config.Value v = (Config.Value)obj;
                
                return QueryUtil.getUusvIdxCsv(v.getNumber() < 224, 
                        v.getV(), v.getEnumGroup().getParentMessage());
            }
        },
        
        ENTITY_IDX_FIELDS
        {
            public Object get(Object obj)
            {
                if (obj instanceof EnumGroup.Value)
                {
                    EnumGroup.Value v = (EnumGroup.Value)obj;
                    
                    return QueryUtil.getUusvFields(v.getNumber() < 224, 
                            v, v.getEnumGroup().getParentMessage());
                }
                
                Config.Value v = (Config.Value)obj;
                
                return QueryUtil.getUusvFields(v.getNumber() < 224, 
                        v.getV(), v.getEnumGroup().getParentMessage());
            }
        },
        
        ENTITY_IDX_FOREIGN_FIELDS
        {
            public Object get(Object obj)
            {
                if (obj instanceof EnumGroup.Value)
                {
                    EnumGroup.Value v = (EnumGroup.Value)obj;
                    
                    return QueryUtil.getUusvForeignFields(v.getNumber() < 224, 
                            v, v.getEnumGroup().getParentMessage());
                }
                
                Config.Value v = (Config.Value)obj;
                
                return QueryUtil.getUusvForeignFields(v.getNumber() < 224, 
                        v.getV(), v.getEnumGroup().getParentMessage());
            }
        },
        
        ENTITY_UPDATE_RES
        {
            public Object get(Object data)
            {
                Message message = (Message)data;
                Annotation entity = message.getAnnotation("Entity");
                
                return entity != null ? entity.getP().get("update_res") : null;
            }
        },
        
        ENTITY_SERIAL_CACHE_TYPE
        {
            public Object get(Object obj)
            {
                if (obj == null)
                    return null;
                Message message = (Message)obj;
                if (!message.getOptions().containsKey("~entity.kind"))
                    return null;
                
                Annotation entity = message.getAnnotation("Entity");
                if (!Boolean.TRUE.equals(entity.getValue("serial")))
                    return null;
                
                Object cache = entity.getValue("cache");
                if (cache == null || Boolean.FALSE.equals(cache))
                    return null;
                
                if (cache instanceof Boolean)
                    return "KVI";
                
                if (cache instanceof String)
                    return cache.toString().replaceAll("Cache", "");
                
                return cache.toString(); 
            }
        },
        
        ENTITY_FROM_SUGGEST
        {

            public Object get(Object obj)
            {
                if (obj == null)
                    return null;
                Field<?> field = (Field<?>)obj;
                
                if (field.getA().containsKey("Id"))
                    return field.getA().get("Id").getP().get("entity");
                
                if (field.getA().containsKey("Key"))
                    return field.getA().get("Key").getP().get("entity");
                
                return null;
            }
            
        },
        
        FILTERED_COMPUTED_FIELDS
        {
            @SuppressWarnings("unchecked")
            public Object get(Object data)
            {
                List<Field<?>> fields = (List<Field<?>>)data;
                
                ArrayList<Field<?>> list = new ArrayList<Field<?>>();
                for (Field<?> f : fields)
                {
                    if (f.getO().containsKey("compute_to_parent"))
                        list.add(f);
                }
                return list;
            }
        },
        
        FILTERED_CTP_OR_MUTABLE_FIELDS
        {
            @SuppressWarnings("unchecked")
            public Object get(Object data)
            {
                List<Field<?>> fields = (List<Field<?>>)data;
                
                ArrayList<Field<?>> list = new ArrayList<Field<?>>();
                for(Field<?> f : fields)
                {
                    if (IsFieldMap.Functions.CTP_OR_MUTABLE.query(f))
                        list.add(f);
                }
                
                return list;
            }
        },
        
        QFORM_VALUES
        {
            public Object get(Object data)
            {
                Config config = (Config)data;
                EnumGroup eg = config.getEg();
                EnumGroup.Value end = (EnumGroup.Value)eg.getO().get("qform_exclude");
                Collection<Config.Value> values = config.getV().values();
                if (end == null)
                    return values;
                
                ArrayList<Config.Value> list = new ArrayList<Config.Value>();
                for (Config.Value v : values)
                {
                    if (v.getV() == end)
                        break;
                    
                    list.add(v);
                }
                return list;
            }
        },
        
        ENTITY_IMMUTABLE_FIELDS
        {
            @Override
            public Object get(Object data)
            {
                Message message = (Message)data;
                
                ArrayList<Field<?>> list = new ArrayList<Field<?>>();
                
                for (Field<?> f : message.getFields())
                {
                    if (f.getNumber() > 2 && !f.isRepeated() && !f.isMessageField() && Boolean.TRUE.equals(f.getOption("immutable")))
                        list.add(f);
                }
                
                return list;
            }
        },
        
        AND_PUT_ENTITY_IMMUTABLE_FIELDS__W
        {
            public Object get(Object obj)
            {
                Writable w = (Writable)obj;
                if (!w.list.isEmpty())
                    throw new CodegenException("Expected empty writable.list but instead the size was: " + w.list.size());
                
                Message message = w.$val(Message.class);
                
                for (Field<?> f : message.getFields())
                {
                    if (f.getNumber() > 2 && !f.isRepeated() && !f.isMessageField() && Boolean.TRUE.equals(f.getOption("immutable")))
                        w.list.add(f);
                }
                
                return w.list;
            }
        },
        
        REQUIRED_FIELDS_BITSET
        {
            @SuppressWarnings("unchecked")
            public Object get(Object obj)
            {
                List<Field<?>> fields = (List<Field<?>>)obj;
                int bitset = 0;
                for (Field<?> f : fields)
                {
                    if (f.isRequired())
                        bitset |= (1 << (f.getNumber() - 1));
                }
                
                return bitset;
            }
        },
        
        REQUIRED_FIELDS
        {
            @SuppressWarnings("unchecked")
            public Object get(Object obj)
            {
                List<Field<?>> fields = (List<Field<?>>)obj;
                ArrayList<Field<?>> list = new ArrayList<Field<?>>();
                for (Field<?> f : fields)
                {
                    if (f.isRequired())
                        list.add(f);
                }
                
                return list;
            }
        },
        
        OPTIONAL_FIELDS
        {
            @SuppressWarnings("unchecked")
            public Object get(Object obj)
            {
                List<Field<?>> fields = (List<Field<?>>)obj;
                ArrayList<Field<?>> list = new ArrayList<Field<?>>();
                for (Field<?> f : fields)
                {
                    if (!f.isRequired())
                        list.add(f);
                }
                
                return list;
            }
        },
        
        CONFIG_FIELDS
        {
            public Object get(Object obj)
            {
                if (obj instanceof EnumGroup.Value)
                    return QueryUtil.getConfigTargetFields((EnumGroup.Value)obj, "f", false);
                
                if (obj instanceof Field<?>)
                    return QueryUtil.getConfigTargetFields((Field<?>)obj, "f", false);
                
                throw err("Codegen error calling get_config_fields.(x) where x is" + 
                        obj.getClass().getSimpleName(), getProto(obj));
            }
        },
        
        PREFIX_CONFIG_FIELDS
        {
            public Object get(Object obj)
            {
                if (obj instanceof EnumGroup.Value)
                    return QueryUtil.getConfigTargetFields((EnumGroup.Value)obj, "f", true);
                
                if (obj instanceof Field<?>)
                    return QueryUtil.getConfigTargetFields((Field<?>)obj, "f", true);
                
                throw err("Codegen error calling get_config_fields.(x) where x is" + 
                        obj.getClass().getSimpleName(), getProto(obj));
            }
        },
        
        MAP_CONFIG_FIELDS
        {
            public Object get(Object obj)
            {
                if (obj instanceof EnumGroup)
                    return QueryUtil.getConfigTargetFieldMap((EnumGroup)obj, "f", false);
                
                if (obj instanceof Message)
                    return QueryUtil.getConfigTargetFieldMap((Message)obj, "f", false);
                
                throw err("Codegen error calling get_map_config_fields.(x) where x is" + 
                        obj.getClass().getSimpleName(), getProto(obj));
            }
        },
        
        MAP_PREFIX_CONFIG_FIELDS
        {
            public Object get(Object obj)
            {
                if (obj instanceof EnumGroup)
                    return QueryUtil.getConfigTargetFieldMap((EnumGroup)obj, "f", true);
                
                if (obj instanceof Message)
                    return QueryUtil.getConfigTargetFieldMap((Message)obj, "f", true);
                
                throw err("Codegen error calling get_map_config_fields.(x) where x is" + 
                        obj.getClass().getSimpleName(), getProto(obj));
            }
        },
        
        MAP_ENTITY_FIELD_VALUE_OFFSET
        {
            public Object get(Object obj)
            {
                Message message = (Message)obj;
                
                final List<Field<?>> orderedFields = (List<Field<?>>)message.getO().get(
                          "~entity.ordered_write_fields");
                if (orderedFields != null)
                {
                    LinkedHashMap<Integer,Field<?>> offsets = 
                            new LinkedHashMap<Integer,Field<?>>(orderedFields.size());
                    for (int i = orderedFields.size(); i-- > 0;)
                    {
                        Field<?> f = orderedFields.get(i);
                        Integer vo = (Integer)f.getOptions().get("~vo");
                        if (vo == null)
                            break;
                        
                        offsets.put(vo, f);
                    }
                    return offsets;
                }
                
                List<Field<?>> sortedFields = message.getFields();
                
                LinkedHashMap<Integer,Field<?>> offsets = 
                        new LinkedHashMap<Integer,Field<?>>(sortedFields.size());
                
                //int lastOffset = 0;
                //Field<?> lastField = null;
                for (int i = sortedFields.size(); i-- > 0;)
                {
                    Field<?> f = sortedFields.get(i);
                    Integer vo = (Integer)f.getOptions().get("~vo");
                    if (vo == null)
                        break;
                    
                    offsets.put(vo, f);
                    /*if (f.isBoolField() || CodegenUtil.isOneByte(f))
                    {
                        if (lastField == null)
                            lastOffset = 1;
                        else
                            lastOffset = 1 + EntityUtil.sizeOf(lastField.getNumber()) + lastOffset;
                        
                        lastField = f;
                        offsets.put(lastOffset, f);
                        
                        continue;
                    }
                    
                    if (f instanceof Field.Fixed32 
                            || f instanceof Field.SFixed32 
                            || f instanceof Field.Float)
                    {
                        if (lastField == null)
                            lastOffset = 4;
                        else
                            lastOffset = 4 + EntityUtil.sizeOf(lastField.getNumber()) + lastOffset;
                        
                        lastField = f;
                        offsets.put(lastOffset, f);
                        
                        continue;
                    }
                    
                    if (f instanceof Field.Fixed64 
                            || f instanceof Field.SFixed64
                            || f instanceof Field.Double)
                    {
                        if (lastField == null)
                            lastOffset = 8;
                        else
                            lastOffset = 8 + EntityUtil.sizeOf(lastField.getNumber()) + lastOffset;
                        
                        lastField = f;
                        offsets.put(lastOffset, f);
                        
                        continue;
                    }
                    
                    if (f.isBytesField() && f.getName().endsWith("key"))
                    {
                        if (lastField == null)
                            lastOffset = 9;
                        else
                            lastOffset = 9 + EntityUtil.sizeOf(lastField.getNumber()) + lastOffset;
                        
                        lastField = f;
                        offsets.put(lastOffset, f);
                        
                        lastOffset++; // adjustment because of the length delimiter
                        
                        continue;
                    }
                    
                    break;*/
                }
                
                return offsets;
            }
        },
        
        VO_METHOD_NAME
        {
            public Object get(Object data)
            {
                Field<?> f = (Field<?>)data;
                if (f.isBoolField())
                    return "Bool";
                
                if (CodegenUtil.isOneByte(f))
                    return "Int8";
                
                if (f.isBytesField())
                    return "Key";
                
                if ("int".equals(f.getJavaType()))
                    return "Int32";
                
                if ("long".equals(f.getJavaType()))
                    return "Int64";
                
                return f.getClass().getSimpleName();
            }
        },
        
        VALIDATED_ENTITY_IDX_STRING
        {
            public Object get(Object data)
            {
                final EnumGroup.Value v = data instanceof Config.Value ? 
                        ((Config.Value)data).getV() : (EnumGroup.Value)data;
                
                final String str = QueryUtil.getUusvIdxString(v.getNumber() < 224, 
                        v, v.getEnumGroup().getParentMessage());
                        
                final Object optionV = v.getO().get("unique_prefix");
                
                if (optionV == null)
                    return str;
                
                final int index;
                if (!(optionV instanceof Integer) || 
                        0 > (index = ((Integer)optionV).intValue()))
                {
                    throw ErrorUtil.err("The unique_prefix of " + 
                            v.getEg().getRelativeName() + "::" + v.getName() + 
                            " must be configured with a positive number (int).", 
                            v);
                }
                
                if (index >= str.length())
                {
                    throw ErrorUtil.err("The unique_prefix of " + 
                            v.getEg().getRelativeName() + "::" + v.getName() + 
                            " is out of bounds (must be less than the total fields).", 
                            v);
                }
                if ('S' != str.charAt(index))
                {
                    throw ErrorUtil.err("The unique_prefix of " + 
                            v.getEg().getRelativeName() + "::" + v.getName() + 
                            " must point to the index where the string field is.", 
                            v);
                }
                
                return str;
            }
        },
        
        AP
        {
            @Override
            public Object get(Object data)
            {
                Writable w = (Writable)data;
                Annotation a = w.$val(Annotation.class);
                String k = w.key.toString();
                Object v = a == null ? null : a.getP().get(k);
                
                // reset
                w.key = null;
                
                w.val = v;
                return v;
            }
        },
        
        MO
        {
            @Override
            public Object get(Object data)
            {
                Writable w = (Writable)data;
                Message m = w.$val(Message.class);
                String k = w.key.toString();
                Object v = m == null ? null : m.getO().get(k);
                
                // reset
                w.key = null;
                
                w.val = v;
                return v;
            }
        },
        
        FO
        {
            @Override
            public Object get(Object data)
            {
                Writable w = (Writable)data;
                Field<?> f = w.$val(Field.class);
                String k = w.key.toString();
                Object v = f == null ? null : f.getO().get(k);
                
                // reset
                w.key = null;
                
                w.val = v;
                return v;
            }
        },
        
        TS_P_ARGS
        {
            public Object get(Object data)
            {
                Writable w = (Writable)data;
                if (w.key == null)
                    throw new CodegenException("Expected the key to be the owner but instead was null");
                
                String ownerName = w.key.toString();
                w.key = null;
                
                return Permutation.toString(w.$val(Permutation.class), ownerName);
            }
        },
        
        TS_OL_P_ARGS
        {
            public Object get(Object data)
            {
                Writable w = (Writable)data;
                if (w.key == null)
                    throw new CodegenException("Expected the key to be the owner but instead was null");
                
                String ownerName = w.key.toString();
                w.key = null;
                
                return Permutation.toStringObjectLiteral(w.$val(Permutation.class), ownerName);
            }
        }
        
        ;
        public final GetMap map;

        private Functions()
        {
            map = new GetMap("get_" + name().toLowerCase(), this);
        }
    }
}
