//========================================================================
//Copyright 2016 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.fbsgen.ds.map;

import com.dyuproject.fbsgen.parser.Annotation;
import com.dyuproject.fbsgen.parser.Field;
import com.dyuproject.fbsgen.parser.Field.PbType;
import com.dyuproject.fbsgen.parser.Message;
import com.dyuproject.fbsgen.parser.MessageField;
import com.dyuproject.fbsgen.parser.ParseException;
import com.dyuproject.fbsgen.parser.Proto;
import com.dyuproject.fbsgen.parser.ProtoUtil;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Represents the annotations for validation on proto files.
 * 
 * @author David Yu
 * @created Jul 30, 2016
 */
public enum AnnotatedValidation
{
    AsciiOnly(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isStringField())
                throw err(a, f, " can only be applied to string fields.");
        }
    },
    AsciiPrintable(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isStringField())
                throw err(a, f, " can only be applied to string fields.");
        }
    },
    AsciiSafeHtml(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isStringField())
                throw err(a, f, " can only be applied to string fields.");
        }
    },
    AssertFalse(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isBoolField())
                throw err(a, f, " can only be applied to bool fields.");
        }
    },
    AssertTrue(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isBoolField())
                throw err(a, f, " can only be applied to bool fields.");
        }
    },
    Key
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isBytesField())
                throw err(a, " cannot be applied to a non-key field: ", f);
            if (!f.isRepeated() && !"key".equals(f.getName()) && 
                    !f.getName().endsWith("_key") && 
                    // only prk fields (p0,p1/etc) are allowed
                    !f.getOwner().getProto().getDeclaredPackageName().startsWith("com.dyuproject.protostuff.ds"))
            {
                throw err(a, " cannot be applied to a key field not ending with '_key': ", f);
            }
            
            Object entity = a.getValue("entity");
            if (entity == null)
            {
                if (!f.getA().containsKey("HasKey"))
                    return;
                
                throw err("@Key.entity on " + f.getOwner().getRelativeName() + 
                        "::key must be declared and point to an Entity " +
                        "(because the owner is annotated with @HasKey).", f.getProto());
            }
            
            if (!(entity instanceof Message))
                throw errV(a, "entity", f, " must point to an entity.");
            
            if (Boolean.TRUE.equals(a.getValue("allow_zk")))
                return;
            
            sb.append(", ").append(((Message)entity).getO().get("~entity.kind"));
        }
    },
    Id(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!IsFieldMap.Functions.ID_TYPE.query(f))
                throw err(a, " cannot be applied to a non-id field: ", f);
            
            Object entity = a.getValue("entity");
            if (entity == null || entity instanceof Message)
                return;
            
            throw errV(a, "entity", f, " must point to an entity.");
        }
    },
    Unique
    {
        // TODO unique enums
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isRepeated())
                throw err(a, f, " can only be applied to a repeated bytes/string/8/16/32/64 field.");
            
            if (f.isBytesField())
                return;
            
            if (f.isStringField())
            {
                sb.append(", (String)null");
                return;
            }
            
            if (f.isNumberField())
            {
                if (f.isFpt())
                    throw err(a, f, " can only be applied to a repeated bytes/string/8/16/32/64 field.");
                
                $appendTo(sb, a, "min");
                // append dummy null
                if ("int".equals(f.getJavaType()))
                    sb.append(", (Integer)null");
                else
                    sb.append(", (Long)null");
                return;
            }
            
            if (!f.isMessageField())
                throw err(a, f, " can only be applied to a repeated bytes/string/8/16/32/64 field.");
            
            MessageField mf = (MessageField)f;
            Message target = mf.getMessage();
            Annotation hasid = target.getAnnotation("HasId"),
                    haskey = target.getAnnotation("HasKey"),
                    haspk = target.getAnnotation("HasParentKey");
            
            Field<?> kf;
            
            if (haspk != null)
            {
                kf = (Field<?>)GetMap.Functions.HASPARENTKEY_FIELD.get(f);
                if (kf == null)
                    throw err(a, f, " did not have a valid HasParentKey link.");
                
                // TODO allow haspk
                //if (hasid != null)
                //    $appendTo(sb, a, "min", "1");
            }
            else if (haskey == null)
            {
                if (hasid != null)
                {
                    $appendTo(sb, a, "min", "1").append(", (com.dyuproject.protostuffdb.HasId)null");
                    return;
                }
                    
                throw err(a, f, " cannot work unless you annotate " + 
                        mf.getMessage().getRelativeName() + 
                        " with @HasId or @HasKey or @HasParentKey.");
            }
            else if (hasid == null)
            {
                kf = (Field<?>)GetMap.Functions.HASKEY_FIELD.get(f);
                if (kf == null)
                    throw err(a, f, " did not have a valid HasKey link.");
            }
            else
            {
                $appendTo(sb, a, "min", "1").append(", message.").append(
                        ProtoUtil.toCamelCase(f.getName()));
                
                kf = (Field<?>)GetMap.Functions.HASKEY_FIELD.get(f);
                if (kf == null)
                    throw err(a, f, " did not have a valid HasKey link.");
            }
            
            Object entity = a.getValue("entity");
            final Annotation key = kf.getAnnotation("Key");
            final String cast = haspk != null ? "HasParentKey" : "HasKey";
            
            if (key != null)
            {
                if (entity != null)
                    throw err(a, f, " cannot have the param: entity.");
                
                entity = key.getValue("entity");
                if (entity == null)
                    sb.append(", 0");
                else if (!(entity instanceof Message))
                    throw err(key, kf, " must point to an entity.");
                else if (sb.length() == $appendTo(sb, ((Message)entity).getO().get("~entity.kind")).length())
                {
                    throw err(((Message)entity).getRelativeName() + 
                            " must be an entity (" + cast + ") as referenced from ", 
                            a, f);
                }
                
                if (haspk != null || hasid == null)
                    sb.append(", (com.dyuproject.protostuffdb.").append(cast).append(")null");
                return;
            }
            
            if (!(entity instanceof Message))
                throw errV(a, "entity", f, " must point to an entity.");
            
            if (sb.length() == $appendTo(sb, ((Message)entity).getO().get("~entity.kind")).length())
            {
                throw err(((Message)entity).getRelativeName() + 
                        " must be an entity (" + cast + ") as referenced from ", 
                        a, f);
            }
            
            if (haspk != null || hasid == null)
                sb.append(", (com.dyuproject.protostuffdb.").append(cast).append(")null");
        }
    },
    UniqueKeys
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isRepeated() || !f.isMessageField())
                throw err(a, f, " can only be applied to repeated message fields.");
            
            MessageField mf = (MessageField)f;
            Message source = mf.getMessage();
            Annotation haskeys = source.getAnnotation("HasKeys");
            if (a.isEmptyP())
            {
                $indexTo(sb, source, haskeys);
                return;
            }
            
            LinkedHashMap<String,Object> params = haskeys.getP();
            HashMap<String, Integer> valueToIndex = new HashMap<String, Integer>();
            int i = 0;
            for (String k = "f" + i; params.containsKey(k); k = "f" + i)
                valueToIndex.put(params.get(k).toString(), i++);
            
            if (i == 0)
            {
                // f0 was not found in the params (contains unreleated params)
                $indexTo(sb, source, haskeys);
                return;
            }
            
            i = 0;
            params = a.getP();
            for (String k = "f" + i; params.containsKey(k); k = "f" + i)
            {
                Integer idx = valueToIndex.get(params.get(k));
                if (idx == null)
                {
                    throw err(a, f, " contains an entry: " + k + 
                            " that does not point to a configured field with respect to @HasKeys on " + 
                            source.getRelativeName());
                }
                
                sb.append(", ").append(idx.toString());
            }
        }
    },
    UniqueIds
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isRepeated() || !f.isMessageField())
                throw err(a, f, " can only be applied to repeated message fields.");
            
            MessageField mf = (MessageField)f;
            Message source = mf.getMessage();
            Annotation hasids = source.getAnnotation("HasIds");
            if (a.isEmptyP())
            {
                $indexTo(sb, source, hasids);
                return;
            }
            
            LinkedHashMap<String,Object> params = hasids.getP();
            HashMap<String, Integer> valueToIndex = new HashMap<String, Integer>();
            int i = 0;
            for (String k = "f" + i; params.containsKey(k); k = "f" + i)
                valueToIndex.put(params.get(k).toString(), i++);
            
            if (i == 0)
            {
                // f0 was not found in the params (contains unreleated params)
                $indexTo(sb, source, hasids);
                return;
            }
            
            i = 0;
            params = a.getP();
            for (String k = "f" + i; params.containsKey(k); k = "f" + i)
            {
                Integer idx = valueToIndex.get(params.get(k));
                if (idx == null)
                {
                    throw err(a, f, " contains an entry: " + k + 
                            " that does not point to a configured field with respect to @HasKeys on " + 
                            source.getRelativeName());
                }
                
                sb.append(", ").append(idx.toString());
            }
        }
    },
    Date(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!IsFieldMap.Functions.DATE.query(f))
                throw err(a, " cannot be applied to a non-date field: ", f);
        }
    },
    Time(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!IsFieldMap.Functions.TIME.query(f))
                throw err(a, " cannot be applied to a non-date field: ", f);
        }
    },
    DecimalMax
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f.isStringField() || f.isFpt())
                $appendTo(sb, f, a, "value");
            else
                throw err(a, f, " can only be applied to string/float/double fields.");
        }
    },
    DecimalMin
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f.isStringField() || f.isFpt())
                $appendTo(sb, f, a, "value");
            else
                throw err(a, f, " can only be applied to string/float/double fields.");
        }
    },
    Digits
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f.isStringField() || f.isNumberField())
                $appendTo($appendTo(sb, f, a, "integer"), f, a, "fraction");
            else
                throw err(a, f, " can only be applied to string/number fields.");
        }
    },
    FutureTS
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f instanceof Field.UInt64 && f.getPbType() == PbType.FIXED64)
                $appendTo($appendTo($appendTo(sb, a, "unit", "1"), a, "min", "0"), a, "max", "0");
            else
                throw err(a, f, " can only be applied to uint64 fields.");
        }
    },
    Future
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f instanceof Field.UInt64 && f.getPbType() == PbType.FIXED64)
                $appendTo($appendTo(sb, a, "min", "0"), a, "max", "0");
            else
                throw err(a, f, " can only be applied to uint64 fields.");
        }
    },
    FutureOrToday
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f instanceof Field.UInt64 && f.getPbType() == PbType.FIXED64)
                $appendTo($appendTo(sb, a, "min", "0"), a, "max", "0");
            else
                throw err(a, f, " can only be applied to uint64 fields.");
        }
    },
    Max
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f.isStringField() || f.isNumberField())
                $appendTo(sb, f, a, "value");
            else
                throw err(a, f, " can only be applied to string/number fields.");
        }
    },
    Min
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f.isStringField() || f.isNumberField())
                $appendTo(sb, f, a, "value");
            else
                throw err(a, f, " can only be applied to string/number fields.");
        }
    },
    GT
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f.isStringField() || f.isNumberField())
                $appendTo(sb, f, a, "value");
            else
                throw err(a, f, " can only be applied to string/number fields.");
        }
    },
    NotNull(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            // let the java compiler do its thing
        }
    },
    NotNullNotEmpty(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isRepeated())
                throw err(a, f, " can only be applied to repeated fields.");
        }
    },
    Null(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            // let the java compiler do its thing
        }
    },
    PastTS
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f instanceof Field.UInt64 && f.getPbType() == PbType.FIXED64)
                $appendTo($appendTo($appendTo(sb, a, "unit", "1"), a, "min", "0"), a, "max", "0");
            else
                throw err(a, f, " can only be applied to uint64 fields.");
        }
    },
    Past
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f instanceof Field.UInt64 && f.getPbType() == PbType.FIXED64)
                $appendTo($appendTo(sb, a, "min", "0"), a, "max", "0");
            else
                throw err(a, f, " can only be applied to uint64 fields.");
        }
    },
    PastOrToday
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f instanceof Field.UInt64 && f.getPbType() == PbType.FIXED64)
                $appendTo($appendTo(sb, a, "min", "0"), a, "max", "0");
            else
                throw err(a, f, " can only be applied to uint64 fields.");
        }
    },
    Pattern
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f.isStringField())
                sb.append(", PATTERN_").append(f.getName());
            else
                throw err(a, f, " can only be applied to string fields.");
        }
    },
    Size
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f.isRepeated() || f.isStringField())
                $appendTo($appendTo(sb, a, "min", "0"), f, a, "max");
            else
                throw err(a, f, " can only be applied to repeated/string fields.");
        }
    },
    CreditCardNumber(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isStringField())
                throw err(a, f, " can only be applied to string fields.");
        }
    },
    Email(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isStringField())
                throw err(a, f, " can only be applied to string fields.");
        }
    },
    Length
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f.isStringField())
                $appendTo($appendTo(sb, a, "min", "0"), f, a, "max");
            else
                throw err(a, f, " can only be applied to string fields.");
        }
    },
    NotBlank(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isStringField())
                throw err(a, f, " can only be applied to string fields.");
        }
    },
    NotEmpty(true)
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (!f.isStringField() && (!(f instanceof MessageField) || 
                    !((MessageField)f).getMessage().getName().endsWith("CAS")))
            {
                throw err(a, f, " can only be applied to string/cas fields.");
            }
        }
    },
    Range
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f.isStringField() || f.isNumberField())
                $appendTo($appendTo(sb, f, a, "min"), f, a, "max");
            else
                throw err(a, f, " can only be applied to string/number fields.");
        }
    },
    URL
    {
        @Override
        public void appendTo(StringBuilder sb, Annotation a, Field<?> f)
        {
            if (f.isStringField())
                $appendTo($appendTo($appendTo(sb, a, "protocol", "http"), f, a, "host"), a, "port");
            else
                throw err(a, f, " can only be applied to string fields.");
        }
    }
    ;
    
    public final boolean skipParams;
    
    private AnnotatedValidation()
    {
        this(false);
    }
    
    private AnnotatedValidation(boolean skipParams)
    {
        this.skipParams = skipParams;
    }

    public abstract void appendTo(StringBuilder sb, Annotation a, Field<?> f);
    
    static void $indexTo(StringBuilder sb, Message m, Annotation a)
    {
        LinkedHashMap<String, Object> params = a.getP();
        int i = 0;
        for (String k = "f" + i; params.containsKey(k); k = "f" + i)
            sb.append(", ").append(i++);
        
        if (i == 0)
            throw err(a, m, " must have atleast one param f0.");
    }
    
    static ParseException errV(Annotation a, String key, Field<?> f, String msg)
    {
        return new ParseException("@" + a.getName() + "." + key + " on " + 
                f.getOwner().getRelativeName() + "::" + f.getName() + msg + 
                " [" + f.getProto().getSourcePath() + "]");
    }
    
    static ParseException err(String msg, Proto proto)
    {
        return new ParseException(msg + 
                " [" + proto.getSourcePath() + "]");
    }
    
    static ParseException err(String msg, Annotation a, Field<?> f)
    {
        return new ParseException(msg + "@" + a.getName() + " on " + 
                f.getOwner().getRelativeName() + "::" + f.getName() +
                " [" + f.getProto().getSourcePath() + "]");
    }
    
    static ParseException err(Annotation a, Message m, String msg)
    {
        return new ParseException("@" + a.getName() + " on " + 
                m.getRelativeName()  + msg + 
                " [" + m.getProto().getSourcePath() + "]");
    }
    
    static ParseException err(Annotation a, Field<?> f, String msg)
    {
        return new ParseException("@" + a.getName() + " on " + 
                f.getOwner().getRelativeName() + "::" + f.getName() + msg + 
                " [" + f.getProto().getSourcePath() + "]");
    }
    
    static ParseException err(Annotation a, String msg, Field<?> f)
    {
        return new ParseException("@" + a.getName() + msg + 
                f.getOwner().getRelativeName() + "::" + f.getName() + 
                " [" + f.getProto().getSourcePath() + "]");
    }
    
    static ParseException err(Field<?> f, String key, String msg)
    {
        return new ParseException(
                f.getOwner().getRelativeName() + "::" + f.getName() + 
                " option: " + key + msg + 
                " [" + f.getProto().getSourcePath() + "]");
    }
    
    static StringBuilder $appendTo(StringBuilder sb, Object nullable)
    {
        return nullable == null ? sb : sb.append(", ").append(nullable.toString());
    }
    
    static StringBuilder $appendTo(StringBuilder sb, Field<?> f, String key)
    {
        Object nullable = f.getO().get(key);
        if (nullable == null)
            throw err(f, key, " must be provided.");
        
        return sb.append(", ").append(nullable.toString());
    }
    
    static StringBuilder $appendTo(StringBuilder sb, Field<?> f, Annotation a, String key)
    {
        Object nullable = a.getValue(key);
        if (nullable == null)
            throw errV(a, key, f, " must be provided.");
        
        return sb.append(", ").append(nullable.toString());
    }
    
    static StringBuilder $appendTo(StringBuilder sb, Annotation a, String key)
    {
        Object nullable = a.getValue(key);
        return nullable == null ? sb : sb.append(", ").append(nullable.toString());
    }
    
    static StringBuilder $appendTo(StringBuilder sb, Annotation a, String key, String def)
    {
        Object nullable = a.getValue(key);
        return sb.append(", ").append(nullable != null ? nullable.toString() : def);
    }
}
