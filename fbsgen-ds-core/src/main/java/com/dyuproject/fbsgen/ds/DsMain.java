//========================================================================
//Copyright 2014 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.fbsgen.ds;

import static com.dyuproject.fbsgen.compiler.map.FakeMapUtil.LIST;
import com.dyuproject.fbsgen.compiler.BatchProtoCompiler;
import com.dyuproject.fbsgen.compiler.CompilerMain;
import com.dyuproject.fbsgen.ds.map.CountMap;
import com.dyuproject.fbsgen.ds.map.FilterEnumValueMap;
import com.dyuproject.fbsgen.ds.map.FilterMap;
import com.dyuproject.fbsgen.ds.map.FormatMap;
import com.dyuproject.fbsgen.ds.map.GetMap;
import com.dyuproject.fbsgen.ds.map.IsFieldMap;
import com.dyuproject.fbsgen.ds.map.IsMap;
import com.dyuproject.fbsgen.ds.map.IsMessageMap;
import com.dyuproject.fbsgen.ds.map.VerifyMap;

/**
 * The main entry point for codegen.
 * 
 * @author David Yu
 * @created Sep 12, 2014
 */
public class DsMain
{
    
    static
    {
        BatchProtoCompiler.setRegistryClass(DsRegistry.class);
        
        CountMap.addAllTo(LIST);
        FilterEnumValueMap.addAllTo(LIST);
        FilterMap.addAllTo(LIST);
        FormatMap.addAllTo(LIST);
        GetMap.addAllTo(LIST);
        IsFieldMap.addAllTo(LIST);
        IsMap.addAllTo(LIST);
        IsMessageMap.addAllTo(LIST);
        VerifyMap.addAllTo(LIST);
        
        Config.initRenderers();
        Verbs.initRenderers();
    }
    
    public static void main(String[] args)
    {
        CompilerMain.main(args);
    }

}
