//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.fbsgen.ds.map;

import static com.dyuproject.fbsgen.parser.AnnotationContainer.err;
import com.dyuproject.fbsgen.compiler.ErrorUtil;
import com.dyuproject.fbsgen.compiler.FakeMap;
import com.dyuproject.fbsgen.compiler.Writable;
import com.dyuproject.fbsgen.parser.Annotation;
import com.dyuproject.fbsgen.parser.EnumGroup;
import com.dyuproject.fbsgen.parser.Field;
import com.dyuproject.fbsgen.parser.Message;

import java.util.List;

/**
 * A map that simply verifies a param and returns it.
 * 
 * @author David Yu
 * @created Jul 26, 2013
 */
public final class VerifyMap extends FakeMap
{
    
    public interface Function
    {
        Object verify(Object obj);
    }

    public final Function func;
    
    public VerifyMap(String name, Function func)
    {
        super(name);
        this.func = func;
    }
    
    public Object get(Object key)
    {
        return func.verify(key);
    }
    
    public static void addAllTo(List<FakeMap> list)
    {
        for (Functions c : Functions.values())
            list.add(c.map);
    }
    
    static int sizeOf(int fieldNumber)
    {
        return fieldNumber > 15 ? 2 : 1;
    }
    
    public enum Functions implements Function
    {
        CONFIG
        {
            public Object verify(Object obj)
            {
                if (obj instanceof EnumGroup)
                {
                    EnumGroup eg = (EnumGroup)obj;
                    EnumGroup.Value v = eg.getValues().get(0);
                    if (v.getNumber() <= 0)
                        throw err(v, " cannot be negative or zero (config value)", eg.getProto());
                    
                    return eg;
                }
                
                if (obj instanceof Message)
                {
                    // TODO
                    
                    return obj;
                }
                
                throw err("codegen error - passed wrong param: " + 
                        String.valueOf(obj), ErrorUtil.getProto(obj));
            }
        },
        
        TAG
        {
            public Object verify(Object obj)
            {
                EnumGroup.Value v = (EnumGroup.Value)obj;
                
                if (v.getNumber() < 1 || v.getNumber() > 223)
                    throw err(v, " must be within 1-223.", v.getEnumGroup().getProto());
                
                return "";
            }
        },
        
        AND_APPEND_AV_PARAMS__W
        {
            @Override
            public Object verify(Object obj)
            {
                Writable w = (Writable)obj;
                
                Annotation a = (Annotation)w.key;
                // always nullify key on writable after use
                w.key = null;
                
                Field<?> f = (Field<?>)w.val;
                
                AnnotatedValidation av = AnnotatedValidation.valueOf(a.getName());
                
                if (av.skipParams)
                {
                    av.appendTo(null, a, f);
                    return w; // return empty string
                }
                
                StringBuilder sb = new StringBuilder();
                av.appendTo(sb, a, f);
                return sb.toString();
            }
        }
        
        ;
        public final VerifyMap map;

        private Functions()
        {
            map = new VerifyMap("verify_" + name().toLowerCase(), this);
        }
    }
}
