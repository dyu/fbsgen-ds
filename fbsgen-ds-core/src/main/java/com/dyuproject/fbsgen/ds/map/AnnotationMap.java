//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.fbsgen.ds.map;

import static com.dyuproject.fbsgen.compiler.AnnotationUtil.count;
import static com.dyuproject.fbsgen.compiler.AnnotationUtil.keys;
import static com.dyuproject.fbsgen.compiler.AnnotationUtil.list;
import static com.dyuproject.fbsgen.compiler.AnnotationUtil.map;
import com.dyuproject.fbsgen.compiler.FakeMap;
import com.dyuproject.fbsgen.parser.AnnotationContainer;

import java.util.List;

/**
 * A map for querying within ST.
 * 
 * @author David Yu
 * @created Mar 27, 2013
 */
public final class AnnotationMap extends FakeMap
{
    
    public interface Function
    {
        Object query(AnnotationContainer target);
    }

    public final Function func;
    
    public AnnotationMap(String name, Function func)
    {
        super(name);
        this.func = func;
    }
    
    public Object get(Object key)
    {
        return func.query((AnnotationContainer)key);
    }
    
    public static void addAllTo(List<FakeMap> list)
    {
        for (Functions c : Functions.values())
            list.add(c.map);
    }
    
    public enum Functions implements Function
    {
        count_Display_entry
        {
            public Object query(AnnotationContainer ac)
            {
                return count(ac, "Display", "entry");
            }
        },
        count_Display_new_entry
        {
            public Object query(AnnotationContainer ac)
            {
                return count(ac, "Display", "entry", "new_");
            }
        },
        count_Display_update_entry
        {
            public Object query(AnnotationContainer ac)
            {
                return count(ac, "Display", "entry", "update_");
            }
        },
        list_Display_entry
        {
            public Object query(AnnotationContainer ac)
            {
                return list(ac, "Display", "entry");
            }
        },
        keys_Display_entry
        {
            public Object query(AnnotationContainer ac)
            {
                return keys(ac, "Display", "entry");
            }
        },
        map_Display_entry
        {
            public Object query(AnnotationContainer ac)
            {
                return map(ac, "Display", "entry");
            }
        }
        ;
        
        public final AnnotationMap map;

        private Functions()
        {
            map = new AnnotationMap("a_" + name(), this);
        }
    }

}
