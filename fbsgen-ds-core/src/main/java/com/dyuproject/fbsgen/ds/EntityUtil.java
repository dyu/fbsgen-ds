//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.fbsgen.ds;

import static com.dyuproject.fbsgen.compiler.CollectionUtil.addTo;
import static com.dyuproject.fbsgen.compiler.ErrorUtil.err;
import static com.dyuproject.fbsgen.ds.QueryUtil.getUusvFields;
import com.dyuproject.fbsgen.compiler.ErrorUtil;
import com.dyuproject.fbsgen.ds.map.IsMessageMap;
import com.dyuproject.fbsgen.parser.Annotation;
import com.dyuproject.fbsgen.parser.CodegenUtil;
import com.dyuproject.fbsgen.parser.EnumGroup;
import com.dyuproject.fbsgen.parser.Field;
import com.dyuproject.fbsgen.parser.Message;
import com.dyuproject.fbsgen.parser.MessageField;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Utils for validating/querying an entity.
 * 
 * @author David Yu
 * @created Dec 12, 2013
 */
public final class EntityUtil
{
    private EntityUtil() {}
    
    static final int ENTITY_MAX_SIZE = 1024 * 63; // 63k (64512)
    static final int ENTITY_FIELD_MAX_VALUE_SIZE = ENTITY_MAX_SIZE - 9 - 3; // ts, value delim size
    static final boolean AUTO_ORDER_FIELDS_ON_WRITE = Boolean.parseBoolean(
              System.getProperty("entity.auto_order_fields_on_write", "false"));
    
    //Field types used on parent-field indexing.
    /*static final int 
            FT_INT32 = 1, // can be enum 
            FT_UINT32 = 2, 
            FT_SINT32 = 3, 
            FT_FIXED32 = 4, 
            FT_SFIXED32 = 5, 
            FT_FLOAT = 6, 
            
            FT_BYTES = 7, 
            FT_BOOL = 8, 
                    
            FT_INT64 = 9, 
            FT_UINT64 = 10, 
            FT_SINT64 = 11, 
            FT_FIXED64 = 12, 
            FT_SFIXED64 = 13,
            FT_DOUBLE = 14, 
            FT_STRING = 15;
    
    private static final HashMap<String,Integer> fieldTypeMapping = 
            new HashMap<String, Integer>();
    
    static
    {
        fieldTypeMapping.put(Field.Int32.class.getSimpleName(), FT_INT32);
        fieldTypeMapping.put(Field.UInt32.class.getSimpleName(), FT_UINT32);
        fieldTypeMapping.put(Field.SInt32.class.getSimpleName(), FT_SINT32);
        fieldTypeMapping.put(Field.Fixed32.class.getSimpleName(), FT_FIXED32);
        fieldTypeMapping.put(Field.SFixed32.class.getSimpleName(), FT_SFIXED32);
        fieldTypeMapping.put(Field.Float.class.getSimpleName(), FT_FLOAT);
        
        fieldTypeMapping.put(Field.Bytes.class.getSimpleName(), FT_BYTES);
        fieldTypeMapping.put(Field.Bool.class.getSimpleName(), FT_BOOL);
        
        fieldTypeMapping.put(Field.Int64.class.getSimpleName(), FT_INT64);
        fieldTypeMapping.put(Field.UInt64.class.getSimpleName(), FT_UINT64);
        fieldTypeMapping.put(Field.SInt64.class.getSimpleName(), FT_SINT64);
        fieldTypeMapping.put(Field.Fixed64.class.getSimpleName(), FT_FIXED64);
        fieldTypeMapping.put(Field.SFixed64.class.getSimpleName(), FT_SFIXED64);
        fieldTypeMapping.put(Field.Double.class.getSimpleName(), FT_DOUBLE);
        fieldTypeMapping.put(Field.String.class.getSimpleName(), FT_STRING);
    }
    
    public static int getFieldType(Field<?> field)
    {
        return fieldTypeMapping.get(field.getClass().getSimpleName()).intValue();
    }
    
    static boolean isVarInt(int fieldType)
    {
        return 0 != (0x03 & fieldType) && 0 == (0x04 & fieldType);
    }
    
    static boolean isVarInt32(int fieldType)
    {
        //return fieldType < 4;
        return 0 != (0x03 & fieldType) && 0 == (0x0C & fieldType);
    }
    
    static boolean isVarInt64(int fieldType)
    {
        return 0 != (0x03 & fieldType) && 8 == (0x0C & fieldType);
    }
    
    static boolean isDelimited(int fieldType)
    {
        return 7 == (0x07 & fieldType);
    }
    
    public static void main(String[] args)
    {
        System.err.println("---------------- varint ");
        for (int i = 0; i < 16;)
            System.err.println(isVarInt(++i) + " " + i);
        
        System.err.println("---------------- delimited ");
        for (int i = 0; i < 16;)
            System.err.println(isDelimited(++i) + " " + i);
        
        System.err.println("---------------- varint32 ");
        for (int i = 0; i < 16;)
            System.err.println(isVarInt32(++i) + " " + i);
        
        System.err.println("---------------- varint64 ");
        for (int i = 0; i < 16;)
            System.err.println(isVarInt64(++i) + " " + i);
    }*/
    
    /*public static int getEntityKind(Message message)
    {
        Annotation entity = message.getAnnotationMap().get("Entity");
        if (entity != null)
            return getKind(entity.getP().get("kind"), message);

        return getKind(message.getOptions().get("entity.kind"), message);
    }*/
    
    public static Message getEntityParent(Message message)
    {
        return (Message)message.getOptions().get("~entity.parent");
        /*Annotation entity = message.getAnnotationMap().get("Entity");
        if (entity != null)
            return entity.getValue("parent");

        return message.getOptions().get("entity.kind") instanceof Integer ? 
                (Message)message.getOptions().get("entity.parent") : null;*/
    }
    
    public static Message getEntityRoot(Message message)
    {
        for (Message parent = getEntityParent(message); parent != null; 
                parent = getEntityParent(message))
        {
            message = parent;
        }
        return message;
    }
    
    public static Message getEntityRootIfNested(Message message)
    {
        if (!message.isNested())
            return message;
        
        for (Message parent = message.getParentMessage(); 
                parent != null && IsMessageMap.Functions.ENTITY.query(parent); 
                parent = message.getParentMessage())
        {
            message = parent;
        }
        return message;
    }
    
    static int getKind(Annotation entity, Message message)
    {
        final Object k = entity.getP().get("kind");
        if (k instanceof Integer)
        {
            int kind = ((Integer)k).intValue();
            if (kind > 0 && kind < 128)
                return kind;
        }
        
        throw err("The entity kind for " + message.getRelativeName() +
                " must be a number(1-127).", message);
    }
    
    static int getMaxSize(Annotation entity, Message message)
    {
        final Object k = entity.getP().get("max_size");
        if (k == null)
            return 0;
        
        int val;
        if (k instanceof Integer &&
                (val = ((Integer)k).intValue()) >= 0 &&
                val <= ENTITY_MAX_SIZE)
        {
            return val;
        }
        
        throw err("The entity max_size for " + message.getRelativeName() +
                " must be a number(0-" + ENTITY_MAX_SIZE + ").", message);
    }
    
    static int resolveMaxSize(Annotation entity, Message message, int computedSize)
    {
        final Object k = entity.getP().get("rel_max_size");
        if (k == null)
            return 0;
        
        int val;
        if (k instanceof Integer &&
                (val = ((Integer)k).intValue()) > 0 &&
                (val + computedSize) <= ENTITY_MAX_SIZE)
        {
            return val + computedSize;
        }
        
        throw err("The entity rel_max_size for " + message.getRelativeName() +
                " must be a number(1-" + (ENTITY_MAX_SIZE - computedSize) + ").", message);
    }
    
    private static int getIntOption(Field<?> f, String key, int max)
    {
        Object v = f.getOptions().get(key);
        if (v == null)
            return 0;
        int val;
        if (v instanceof Integer &&
                (val = ((Integer)v).intValue()) > 0 && 
                val <= max)
        {
            return val;
        }
        
        throw err(f.getOwner().getRelativeName() + "::" + f.getName() + " " + key +
                " must be a number(1-" + max + ").", f.getOwner());
    }
    
    private static int varintSize(int value)
    {
        if ((value & (0xffffffff <<  7)) == 0) return 1;
        if ((value & (0xffffffff << 14)) == 0) return 2;
        if ((value & (0xffffffff << 21)) == 0) return 3;
        if ((value & (0xffffffff << 28)) == 0) return 4;
        return 5;
    }
    
    public static int sizeOf(int fieldNumber)
    {
        return fieldNumber > 15 ? 2 : 1;
    }
    
    /**
     * Returns null if the message is not an entity.
     * Returns the parent if the entity has a parent, otherwise it returns 
     * the message itself.
     */
    public static Message fill(final Message message)
    {
        if (message.getOptions().containsKey("~entity.kind"))
        {
            Message parent = (Message)message.getOptions().get("~entity.parent");
            return parent == null ? message : parent;
        }
        
        final Annotation entity = message.getAnnotationMap().get("Entity");
        if (entity == null)
            return null;
        /*if (entity != null)
        {
            kind = getKind(entity.getP().get("kind"), message);
            p = entity.getParams().get("parent");
            withLinkIndex = (Boolean)entity.getParams().get("with_link_index");
        }
        else
        {
            final Object k = message.getOptions().get("entity.kind");
            if (k == null)
                return null;
            
            kind = getKind(k, message);
            p = message.getOptions().get("entity.parent");
            withLinkIndex = (Boolean)message.getOptions().get("with_link_index");
        }*/
        
        final int kind = getKind(entity, message);
        final Object p = entity.getParams().get("parent");
        final Boolean withLinkIndex = entity.getValue("with_link_index");
        final Object cache = entity.getValue("cache");
        
        final boolean deriveMaxSize = Boolean.TRUE.equals(entity.getP().get("derive_max_size"));
        final boolean checkStableUpdateSize = deriveMaxSize || Boolean.TRUE.equals(cache);
        int maxSize = deriveMaxSize ? 0 : getMaxSize(entity, message);
        
        Field<?> rev = null;
        if (Boolean.TRUE.equals(entity.getValue("rev")))
        {
            rev = message.getField("rev");
            if (rev == null || !rev.getJavaType().equals("int"))
            {
                throw err("The revisioned entity: " + message.getRelativeName() + 
                        " must have the field: rev (int)", 
                        message);
            }
            
            message.getOptions().put("~entity.rev", rev);
        }
        
        Field.UInt64 updateTs = null;
        if (Boolean.TRUE.equals(entity.getValue("update_ts")))
        {
            Object f = message.getField("update_ts");
            if (f == null || !(f instanceof Field.UInt64) || 
                    !Boolean.TRUE.equals((updateTs = (Field.UInt64)f).getO().get("datetime")))
            {
                throw err("The updateTsisioned entity: " + message.getRelativeName() + 
                        " must have the field: update_ts (fixed64, datetime)", 
                        message);
            }
            
            message.getOptions().put("~entity.update_ts", updateTs);
        }
        
        if (updateTs != null || rev != null)
            message.getOptions().put("~entity.computed_update", true);
        
        if (cache != null && !Boolean.FALSE.equals(cache))
        {
            Field<?> id = message.getField("id");
            if (id == null || !id.getJavaType().equals("int"))
            {
                throw err("The cached entity: " + message.getRelativeName() + 
                        " must have the field: id (int)", 
                        message);
            }
        }
        
        Field<?> f;
        if (message.getFieldCount() < 3 || 
                // key
                !(f=message.getFields().get(0)).isOptional() || 
                !f.isBytesField() || 
                !f.getName().equals("key") || f.getNumber() != 1 ||
                // ts
                !(f=message.getFields().get(1)).isOptional() || 
                !(f instanceof Field.UInt64) || 
                !f.getName().equals("ts") || f.getNumber() != 2)
        {
            throw err("The entity: " + message.getRelativeName() + 
                    " must have atleast 3 fields with these first two declared:" + 
                    "\noptional bytes key = 1 [provided = true, immutable = true];" + 
                    "\noptional fixed64 ts = 2 [provided = true, immutable = true, datetime = true];", 
                    message);
        }
        
        if (message.getRepeatedFieldCount() != 0)
        {
            throw err("The entity: " + message.getRelativeName() + 
                    " cannot have repeated fields.", message);
        }
        
        EnumGroup si = message.getNestedEnumGroup("SI");
        if (si != null && !si.getA().containsKey("Config"))
        {
            throw err("The entity: " + message.getRelativeName() + 
                    " must annotate the nested enum SI with @Config", 
                    message);
        }
        
        int number, delimSize, fieldValueMaxSize;
        int totalFieldMaxValueSize = 0;
        int lastFieldNumberSize = 0;
        int partialSize = 0;
        int lastOffset = 0;
        boolean checkValueOffset = true;
        final ArrayList<Field<?>> varFields = !AUTO_ORDER_FIELDS_ON_WRITE ? null : new ArrayList<Field<?>>();
        final ArrayList<Field<?>> fixedFields = !AUTO_ORDER_FIELDS_ON_WRITE ? null : new ArrayList<Field<?>>();
        int unstableUpdateFieldCount = 0;
        for (int i = message.getFieldCount(); i-->2;)
        {
            f = message.getFields().get(i);
            number = f.getNumber();
            if (number < 3 || number > 127)
            {
                throw err("The entity: " + message.getRelativeName() + 
                        " contains an invalid field: " + f.getName() + 
                        " (its number must be from 3 to 127)", 
                        message);
            }
            
            if (f instanceof MessageField)
            {
                throw err("The entity: " + message.getRelativeName() + 
                        " cannot have a message field: " + f.getName(), 
                        message);
            }
            
            if (f.isOptional() && !f.isDefaultValueSet())
            {
                throw err("The entity: " + message.getRelativeName() + 
                        " must define a default value for the field: " + f.getName(), 
                        message);
            }
            
            if (AUTO_ORDER_FIELDS_ON_WRITE)
            {
                if (f.isBoolField() || CodegenUtil.isOneByte(f))
                {
                    fixedFields.add(f); 
                    lastFieldNumberSize = sizeOf(number);
                    partialSize += (lastFieldNumberSize + 1);
                }
                else if (f instanceof Field.UInt32 || f instanceof Field.Float)
                {
                    fixedFields.add(f);
                    lastFieldNumberSize = sizeOf(number);
                    partialSize += (lastFieldNumberSize + 4);
                }
                else if (f instanceof Field.UInt64 || f instanceof Field.Double)
                {
                    fixedFields.add(f);
                    lastFieldNumberSize = sizeOf(number);
                    partialSize += (lastFieldNumberSize + 8);
                }
                else if (!f.isDelimited())
                {
                    varFields.add(f);
                    partialSize += (sizeOf(number) + 1);
                }
                else if (f.isBytesField() && f.getName().endsWith("_key"))
                {
                    fixedFields.add(f);
                    lastFieldNumberSize = sizeOf(number);
                    partialSize += (lastFieldNumberSize + 1 + 9);
                }
                else if (0 != (fieldValueMaxSize = getIntOption(f, "value_size", ENTITY_FIELD_MAX_VALUE_SIZE)))
                {
                    fixedFields.add(f);
                    lastFieldNumberSize = sizeOf(number);
                    delimSize = varintSize(fieldValueMaxSize);
                    partialSize += (lastFieldNumberSize + delimSize + fieldValueMaxSize);
                }
                else if (!checkStableUpdateSize)
                {
                    varFields.add(f);
                    partialSize += (sizeOf(number) + 1);
                }
                else if (0 != (fieldValueMaxSize = getIntOption(f, "max_value_size", ENTITY_FIELD_MAX_VALUE_SIZE)))
                {
                    varFields.add(f);
                    totalFieldMaxValueSize += fieldValueMaxSize;
                    partialSize += (sizeOf(number) + 1);
                }
                else if (deriveMaxSize)
                {
                    throw err(message.getRelativeName() + "::" + f.getName() +
                            " must provide the option: value_size or max_value_size", message);
                }
                else if (!Boolean.TRUE.equals(f.getO().get("immutable")) &&
                        !Boolean.TRUE.equals(f.getO().get("fixed")))
                {
                    varFields.add(f);
                    unstableUpdateFieldCount++;
                    partialSize += (sizeOf(number) + 1);
                }
                else
                {
                    varFields.add(f);
                    partialSize += (sizeOf(number) + 1);
                }
                continue;
            }
            if (!checkValueOffset)
            {
                if (!f.isDelimited())
                {
                    throw err(message.getRelativeName() + "::" + f.getName() +
                            " should be after the variable-length fields (string/bytes).", 
                            message);
                }
                if (!checkStableUpdateSize)
                {
                    // noop
                }
                else if (0 != (fieldValueMaxSize = getIntOption(f, "value_size", ENTITY_FIELD_MAX_VALUE_SIZE)) ||
                        0 != (fieldValueMaxSize = getIntOption(f, "max_value_size", ENTITY_FIELD_MAX_VALUE_SIZE)))
                {
                    totalFieldMaxValueSize += fieldValueMaxSize;
                }
                else if (deriveMaxSize)
                {
                    throw err(message.getRelativeName() + "::" + f.getName() +
                            " must provide the option: value_size or max_value_size", message);
                }
                else if (!Boolean.TRUE.equals(f.getO().get("immutable")) &&
                        !Boolean.TRUE.equals(f.getO().get("fixed")))
                {
                    unstableUpdateFieldCount++;
                }                
                // field number size + min delim size (empty field value)
                partialSize += (sizeOf(number) + 1);
                continue;
            }
            
            if (f.isBoolField() || CodegenUtil.isOneByte(f))
            {
                lastOffset = lastFieldNumberSize == 0 ? 1 : 1 + lastFieldNumberSize + lastOffset;
                
                lastFieldNumberSize = sizeOf(number);
                partialSize += (lastFieldNumberSize + 1);
                
                f.getOptions().put("~vo", lastOffset);
                f.getOptions().put("~volen", 1);
                
                continue;
            }
            
            if (f instanceof Field.UInt32 
                    || f instanceof Field.Float)
            {
                lastOffset = lastFieldNumberSize == 0 ? 4 : 4 + lastFieldNumberSize + lastOffset;
                
                lastFieldNumberSize = sizeOf(number);
                partialSize += (lastFieldNumberSize + 4);
                
                f.getOptions().put("~vo", lastOffset);
                f.getOptions().put("~volen", 4);
                
                continue;
            }
            
            if (f instanceof Field.UInt64 
                    || f instanceof Field.Double)
            {
                lastOffset = lastFieldNumberSize == 0 ? 8 : 8 + lastFieldNumberSize + lastOffset;
                
                lastFieldNumberSize = sizeOf(number);
                partialSize += (lastFieldNumberSize + 8);
                
                f.getOptions().put("~vo", lastOffset);
                f.getOptions().put("~volen", 8);
                
                if (f instanceof Field.UInt64 && (Boolean.TRUE.equals(
                        f.getOptions().get("date")) || Boolean.TRUE.equals(
                                f.getOptions().get("datetime"))))
                {
                    f.getOptions().put("~volen_on_idx_key", 6);
                }
                
                continue;
            }
            
            if (!f.isDelimited())
            {
                throw err(message.getRelativeName() + "::" + f.getName() +
                        " should be a fixed-width number field: uint32/uint64/float/double or enum with @int8 annotation.", 
                        message);
            }
            
            if (f.isBytesField() && f.getName().endsWith("_key"))
            {
                lastOffset = lastFieldNumberSize == 0 ? 9 : 9 + lastFieldNumberSize + lastOffset;
                
                lastFieldNumberSize = sizeOf(number);
                partialSize += (lastFieldNumberSize + 1 + 9);
                
                f.getOptions().put("~vo", lastOffset);
                f.getOptions().put("~volen", 9);
                
                lastOffset++; // take into account the length delimiter
                
                continue;
            }
            
            if (0 != (fieldValueMaxSize = getIntOption(f, "value_size", ENTITY_FIELD_MAX_VALUE_SIZE)))
            {
                lastOffset = lastFieldNumberSize == 0 ? fieldValueMaxSize : fieldValueMaxSize + lastFieldNumberSize + lastOffset;
                
                lastFieldNumberSize = sizeOf(number);
                delimSize = varintSize(fieldValueMaxSize);
                partialSize += (lastFieldNumberSize + delimSize + fieldValueMaxSize);
                
                f.getOptions().put("~vo", lastOffset);
                f.getOptions().put("~volen", fieldValueMaxSize);
                
                lastOffset += delimSize; // take into account the length delimiter
                continue;
            }
            
            if (!checkStableUpdateSize)
            {
                // noop
            }
            else if (0 != (fieldValueMaxSize = getIntOption(f, "max_value_size", ENTITY_FIELD_MAX_VALUE_SIZE)))
            {
                totalFieldMaxValueSize += fieldValueMaxSize;
            }
            else if (deriveMaxSize)
            {
                throw err(message.getRelativeName() + "::" + f.getName() +
                        " must provide the option: value_size or max_value_size", message);
            }
            else if (!Boolean.TRUE.equals(f.getO().get("immutable")) &&
                    !Boolean.TRUE.equals(f.getO().get("fixed")))
            {
                unstableUpdateFieldCount++;
            }
            
            checkValueOffset = false;
            partialSize += (sizeOf(number) + 1); // field number size + delim size (if string/bytes size is less than 16).
        }
        
        if (Boolean.TRUE.equals(entity.getValue("seq")))
            partialSize += 9; // field number size + min delim size (empty field value)
        
        if (!checkStableUpdateSize)
        {
            // noop
        }
        else if ((partialSize + totalFieldMaxValueSize) > ENTITY_MAX_SIZE)
        {
            throw err("The derived max_size for " + message.getRelativeName() +
                    " is too large: " + (partialSize + totalFieldMaxValueSize) +
                    " > " + ENTITY_MAX_SIZE, message);
        }
        else
        {
            message.getO().put("~entity.stable_update_size", unstableUpdateFieldCount == 0);
        }
        
        if (AUTO_ORDER_FIELDS_ON_WRITE)
        {
            final ArrayList<Field<?>> orderedFields = new ArrayList<Field<?>>();
            message.getO().put("~entity.ordered_write_fields", orderedFields);
            lastFieldNumberSize = 0;
            for (int i = fixedFields.size(); i-- > 0;)
            {
                f = fixedFields.get(i);
                number = f.getNumber();
                if (f.isBoolField() || CodegenUtil.isOneByte(f))
                {
                    lastOffset = lastFieldNumberSize == 0 ? 1 : 1 + lastFieldNumberSize + lastOffset;
                    lastFieldNumberSize = sizeOf(number);
                    f.getOptions().put("~vo", lastOffset);
                    f.getOptions().put("~volen", 1);
                }
                else if (f instanceof Field.UInt32 || f instanceof Field.Float)
                {
                    lastOffset = lastFieldNumberSize == 0 ? 4 : 4 + lastFieldNumberSize + lastOffset;
                    lastFieldNumberSize = sizeOf(number);
                    f.getOptions().put("~vo", lastOffset);
                    f.getOptions().put("~volen", 4);
                }
                else if (f instanceof Field.UInt64 || f instanceof Field.Double)
                {
                    lastOffset = lastFieldNumberSize == 0 ? 8 : 8 + lastFieldNumberSize + lastOffset;
                    lastFieldNumberSize = sizeOf(number);
                    f.getOptions().put("~vo", lastOffset);
                    f.getOptions().put("~volen", 8);
                }
                else if (f.isBytesField() && f.getName().endsWith("_key"))
                {
                    lastOffset = lastFieldNumberSize == 0 ? 9 : 9 + lastFieldNumberSize + lastOffset;
                    lastFieldNumberSize = sizeOf(number);
                    f.getOptions().put("~vo", lastOffset);
                    f.getOptions().put("~volen", 9);
                    lastOffset++; // take into account the length delimiter
                }
                else
                {
                    fieldValueMaxSize = getIntOption(f, "value_size", ENTITY_FIELD_MAX_VALUE_SIZE);
                    lastOffset = lastFieldNumberSize == 0 ? fieldValueMaxSize : fieldValueMaxSize + lastFieldNumberSize + lastOffset;
                    lastFieldNumberSize = sizeOf(number);
                    f.getOptions().put("~vo", lastOffset);
                    f.getOptions().put("~volen", fieldValueMaxSize);
                    lastOffset += varintSize(fieldValueMaxSize); // take into account the length delimiter
                }
            }
            for (int i = varFields.size(); i-- > 0;)
            {
                orderedFields.add(varFields.get(i));
            }
            for (int i = 0, len = fixedFields.size(); i < len; i++)
            {
                orderedFields.add(fixedFields.get(i));
            }
        }
        
        if (deriveMaxSize)
        {
            maxSize = partialSize + totalFieldMaxValueSize;
        }
        else if (maxSize == 0)
        {
            maxSize = resolveMaxSize(entity, message, partialSize + totalFieldMaxValueSize);
        }
        else if (maxSize < (partialSize + totalFieldMaxValueSize))
        {
            throw err("The entity max_size for " + message.getRelativeName() +
                    " must not be lesser than " + (partialSize + totalFieldMaxValueSize), message);
        }
        
        message.getOptions().put("~entity.kind", kind);
        message.getOptions().put("~entity.max_size", maxSize);
        message.getOptions().put("~entity.min_size", partialSize);
        if (withLinkIndex != null && withLinkIndex.booleanValue())
            message.getOptions().put("~entity.with_link_index", Boolean.TRUE);
        
        if (p != null)
        {
            if (!(p instanceof Message))
            {
                throw err("The parent of " + 
                        message.getRelativeName() + " must be another entity.", message);
            }
            
            final Message parent = (Message)p;
            if (parent != message && (message.getAnnotationMap().containsKey("Entity") || 
                    message.getOptions().containsKey("entity.kind")))
            {
                Integer parentKind = parent.getA().get("Entity").getValue("kind");
                if (parentKind != null && parentKind.intValue() == 127)
                {
                    throw err("The parent of " + 
                            message.getRelativeName() + " must not have a kind = 127.", message);
                }
                
                message.getOptions().put("~entity.parent", parent);
                
                if (message.getName().endsWith("Entry"))
                    fillEntry(message, parent, entity, rev, updateTs);
                
                return parent;
            }
            
            throw err("The parent of " + 
                    message.getRelativeName() + " must be another entity.", message);
        }
        
        return message;
    }
    
    static void fillEntry(Message message, Message parent, Annotation entity, 
            Field<?> rev, Field.UInt64 updateTs)
    {
        if (!entity.getP().containsKey("create") && !entity.getP().containsKey("update"))
            return;
        
        addTo(parent.getOptions(), message, "~entity.entry_list");
        
        Object siUnique = entity.getValue("si_unique");
        if (siUnique != null)
        {
            EnumGroup.Value v;
            if (!(siUnique instanceof EnumGroup.Value) || 
                    !(v = (EnumGroup.Value)siUnique).getEg().getName().equals("SI"))
            {
                throw err("@Entity.si_unique of " + 
                        message.getRelativeName() + 
                        " must point to an enum value (SI.FOO).", message);
            }
            
            ArrayList<Field<?>> ff = getUusvFields(v.getNumber() < 224, 
                    v, v.getEnumGroup().getParentMessage());
            
            if (!ff.isEmpty())
            {
                message.getO().put("~entry.si_unique_ff", ff);
            }
        }
        
        Object computeToParent = entity.getValue("compute_to_parent");
        if (computeToParent != null)
        {
            final Field<?> field;
            if (computeToParent instanceof Boolean)
            {
                if (null == (field = parent.getField("total_entry_count")))
                {
                    throw ErrorUtil.err("@Entity.compute_to_parent of " + 
                            message.getRelativeName() + 
                            " was configured but the parent does not have the field: total_entry_count", 
                            message);
                }
            }
            else if (computeToParent instanceof String)
            {
                if (null == (field = parent.getField(computeToParent.toString())))
                {
                    throw ErrorUtil.err("@Entity.compute_to_parent of " + 
                            message.getRelativeName() + 
                            " must point to a parent field: " + computeToParent.toString(), 
                            message);
                }
            }
            else
            {
                throw ErrorUtil.err("@Entity.compute_to_parent of " + 
                        message.getRelativeName() + 
                        " must either be true or a string that points to a parent field", 
                        message);
            }
            
            addTo(message.getOptions(), field, "~entry.computed_list");
        }
        
        
        LinkedHashMap<String, ArrayList<Field<?>>> srcFieldMapping = 
                new LinkedHashMap<String, ArrayList<Field<?>>>();
        LinkedHashMap<Message, ArrayList<Field<?>>> srcProvidedFieldMapping = null;
        //ArrayList<EnumGroup.Value> srcSIList = null;
        Object src = null;
        int computedCount = 0;
        HashMap<String,Message> nonProvided = new HashMap<String,Message>();
        for (Field<?> f : message.getFields())
        {
            if (f.getNumber() < 2)
                continue;
            
            if ((src = f.getO().get("src")) == null)
            {
                if (f.getOptions().containsKey("compute_to_parent"))
                {
                    addTo(message.getOptions(), f, "~entry.computed_list");
                    computedCount++;
                }
                
                continue;
            }
            
            if (src instanceof EnumGroup.Value)
            {
                EnumGroup.Value v = (EnumGroup.Value)src;
                if (Boolean.TRUE.equals(v.getO().get("unique")))
                {
                    message.getOptions().put("~entry.src_si_unique", 
                            Boolean.TRUE);
                }
                else if (v.getO().containsKey("unique_prefix"))
                {
                    message.getOptions().put("~entry.src_si_unique_prefix", 
                            Boolean.TRUE);
                }
                else
                {
                    throw ErrorUtil.err("The @Entity annotation of " + 
                            message.getRelativeName() + 
                            " contains a field: " + f.getName() + 
                            " with a non-unique SI: " + 
                            v.getEg().getRelativeName() + "::" + 
                            v.getName(), message);
                }
                
                message.getOptions().put("~entry.src_si", Boolean.TRUE);
                addTo(srcFieldMapping, f, "SI_" + v.getName());
                
                /*if (srcSIList == null)
                    srcSIList = new ArrayList<EnumGroup.Value>();
                
                srcSIList.add(v);*/
            }
            else if (src instanceof Message)
            {
                Message m = (Message)src;
                String relativeName = m.getRelativeName();
                
                Object mapping = entity.getValue(relativeName);
                if (mapping != null && !(entity.getValue(relativeName) instanceof Message))
                {
                    throw ErrorUtil.err("The @Entity annotation of " + 
                            message.getRelativeName() + 
                            " contains a mapping: " + relativeName + 
                            " that does not point to a message.", message);
                }
                
                addTo(srcFieldMapping, f, relativeName);
                
                if (Boolean.TRUE.equals(entity.getValue(relativeName + ".provided")))
                {
                    if (srcProvidedFieldMapping == null)
                        srcProvidedFieldMapping = new LinkedHashMap<Message, ArrayList<Field<?>>>();
                    
                    addTo(srcProvidedFieldMapping, f, m);
                }
                else if (!nonProvided.containsKey(relativeName))
                {
                    nonProvided.put(relativeName, m);
                    addTo(message.getOptions(), m, "~entry.src_non_provided_list");
                }
            }
            else
            {
                throw ErrorUtil.err("The src option of " + 
                        message.getRelativeName() + "::" + f.getName() + 
                        " must be a message.", message);
            }
            
            if (f.getOptions().containsKey("compute_to_parent"))
            {
                addTo(message.getOptions(), f, "~entry.computed_list");
                computedCount++;
            }
        }
        
        message.getOptions().put("~entry.src_map", srcFieldMapping);
        
        if (srcProvidedFieldMapping != null)
            message.getOptions().put("~entry.src_provided_map", srcProvidedFieldMapping);
        
        /*if (srcSIList != null)
        {
            for (EnumGroup.Value v : srcSIList)
            {
                ArrayList<Field<?>> ff = getUusvFields(v.getNumber() < 224, 
                        v, v.getEnumGroup().getParentMessage());
                
                if (ff.isEmpty())
                    continue;
                
                // TODO find parent field references in src SI
            }
        }*/
        
        if (computedCount != 0 || computeToParent != null || rev != null || updateTs != null)
            message.getOptions().put("~entry.computed_update", true);
    }
}