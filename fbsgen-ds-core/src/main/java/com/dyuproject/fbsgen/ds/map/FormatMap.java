//========================================================================
//Copyright 2012 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.fbsgen.ds.map;

import java.util.List;

import com.dyuproject.fbsgen.compiler.FakeMap;
import com.dyuproject.fbsgen.parser.Message;
import com.dyuproject.fbsgen.parser.ProtoUtil;

/**
 * Codegen helper for formatting values
 * 
 * @author David Yu
 * @created Oct 30, 2012
 */
public final class FormatMap extends FakeMap
{
    
    public interface Function
    {
        Object format(Object data);
    }

    public final Function func;
    
    public FormatMap(String name, Function func)
    {
        super(name);
        this.func = func;
    }
    
    public Object get(Object key)
    {
        return func.format(key);
    }
    
    public static void addAllTo(List<FakeMap> list)
    {
        for (Functions c : Functions.values())
            list.add(c.map);
    }
    
    static int sizeOf(int fieldNumber)
    {
        return fieldNumber > 15 ? 2 : 1;
    }
    
    public enum Functions implements Function
    {
        TO_FIELD_KEY
        {
            public Object format(Object data)
            {
                Message message = (Message)data;
                
                String alias = message.getAnnotation("Entity").getValue("key_alias");
                
                return alias != null ? alias : ProtoUtil.toUnderscoreCase(
                        message.getName()).append("_key").toString();
            }
        },
        
        TO_FIELD_ID
        {
            public Object format(Object data)
            {
                Message message = (Message)data;
                
                String alias = message.getAnnotation("Entity").getValue("id_alias");
                
                return alias != null ? alias : ProtoUtil.toUnderscoreCase(
                        message.getName()).append("_id").toString();
            }
        },
        
        TO_LITERAL_JAVA_INT_ARRAY
        {
            public Object format(Object data)
            {
                @SuppressWarnings("unchecked")
                final List<Message> children = (List<Message>)data;
                final StringBuilder sb = new StringBuilder()
                    .append("new int[]{")
                    .append(((Integer)children.get(0).getOptions().get(
                            "~entity.kind")).intValue());
                
                for (int i = 1, len = children.size(); i < len; i++)
                {
                    sb.append(',').append(((Integer)children.get(i).getOptions().get(
                            "~entity.kind")).intValue());
                }
                
                return sb.append('}');
            }
        },
        
        TO_DART_PKG_IMPORT
        {
            // bookmarks.user => bookmarks/bookmarks/user.dart as bookmarks_user
            public Object format(Object data)
            {
                String pkg = (String)data;
                int dot = pkg.indexOf('.');
                return new StringBuilder()
                        .append("'package:")
                        .append(dot == -1 ? pkg : pkg.substring(0, dot))
                        .append("/g/")
                        .append(pkg.replace('.', '/'))
                        .append(".dart' as ")
                        .append(pkg.replace('.', '_'))
                        .toString();
            }
        }
        
        ;
        
        public final FormatMap map;

        private Functions()
        {
            map = new FormatMap("format_" + name().toLowerCase(), this);
        }
    }
}
