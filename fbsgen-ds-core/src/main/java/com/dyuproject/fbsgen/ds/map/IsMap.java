//========================================================================
//Copyright 2013 David Yu
//------------------------------------------------------------------------
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at 
//http://www.apache.org/licenses/LICENSE-2.0
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
//========================================================================

package com.dyuproject.fbsgen.ds.map;

import static com.dyuproject.fbsgen.compiler.ErrorUtil.err;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.dyuproject.fbsgen.compiler.FakeMap;
import com.dyuproject.fbsgen.compiler.JetGroup;
import com.dyuproject.fbsgen.compiler.Writable;
import com.dyuproject.fbsgen.parser.Annotation;
import com.dyuproject.fbsgen.parser.EnumGroup;
import com.dyuproject.fbsgen.parser.Field;
import com.dyuproject.fbsgen.parser.HasName;
import com.dyuproject.fbsgen.parser.Message;
import com.dyuproject.fbsgen.parser.Proto;
import com.dyuproject.fbsgen.parser.Service;
import com.dyuproject.fbsgen.parser.Service.RpcMethod;

/**
 * A map that returns true/false based the param.
 * 
 * @author David Yu
 * @created Jul 25, 2013
 */
public final class IsMap extends FakeMap
{
    
    public interface Function
    {
        boolean is(Object data);
    }

    public final Function func;
    
    public IsMap(String name, Function func)
    {
        super(name);
        this.func = func;
    }
    
    public Object get(Object key)
    {
        return func.is(key) ? Boolean.TRUE : Boolean.FALSE;
    }
    
    public static void addAllTo(List<FakeMap> list)
    {
        for (Functions c : Functions.values())
            list.add(c.map);
    }
    
    static int sizeOf(int fieldNumber)
    {
        return fieldNumber > 15 ? 2 : 1;
    }
    
    public enum Functions implements Function
    {
        RPC_READ_ONLY
        {
            public boolean is(Object data)
            {
                final RpcMethod rm;
                if (!(data instanceof RpcMethod) || (rm = (RpcMethod)data).getA().containsKey("ReadOnly"))
                    return true;
                
                String name = rm.getName();
                
                return name.startsWith("get") || name.startsWith("list") || 
                        name.startsWith("find") || 
                        // q followed by [A-Z]
                        (name.charAt(0) == 'q' && name.charAt(1) > 96);
            }
        },
        
        SERVICE_EXCLUDE_CLIENT
        {
            public boolean is(Object data)
            {
                Service service = (Service)data;
                if (JetGroup.Base.is_exclude_client(service))
                    return true;
                
                Proto proto = service.getProto();
                Object link = proto.getO().get("link");
                EnumGroup.Value value;
                Annotation exclude;
                return link instanceof EnumGroup &&
                        null != (value = ((EnumGroup)link).getValue(service.getName())) &&
                        null != (exclude = value.getAnnotation("Exclude")) &&
                        Boolean.TRUE.equals(exclude.getValue("client"));
            }
        },
        
        AUTH_REQUIRED
        {
            public boolean is(Object data)
            {
                final EnumGroup.Value v;
                if (data instanceof Service)
                {
                    Service service = (Service)data;
                    Proto proto = service.getProto();
                    EnumGroup serviceGroup = (EnumGroup)proto.getO().get("link");
                    if (serviceGroup == null)
                        throw err(proto, "option link = $service; must be defined in proto file.");
                    
                    v = serviceGroup.getValue(service.getName());
                }
                else
                {
                    v = (EnumGroup.Value)data;
                }
                
                
                Boolean b = (Boolean)v.getOptions().get("auth_required");
                
                if (b != null)
                    return b.booleanValue();
                
                Annotation a = v.getEnumGroup().getAnnotation("ServiceGroup");
                return a != null && Boolean.TRUE.equals(a.getValue("auth_required"));
            }
        },
        
        VALUE_ANY
        {
            public boolean is(Object data)
            {
                EnumGroup.Value v = (EnumGroup.Value)data;
                return Boolean.TRUE.equals(v.getO().get("any")) || 
                        v.getName().endsWith("ANY");
            }
        },
        
        ENTITY
        {
            public boolean is(Object data)
            {
                return data instanceof Message && IsMessageMap.Functions.ENTITY.query(
                        (Message)data);
            }
        },
        
        ENTITY_OTHER_UPDATE_RES
        {
            public boolean is(Object data)
            {
                Message message = (Message)data;
                Annotation entity = message.getAnnotation("Entity");
                if (entity == null)
                    return false;
                
                Object other = entity.getP().get("update_res");
                return other != null && other != data;
            }
        },
        
        ENTITY_SERIAL_CACHE
        {
            public boolean is(Object data)
            {
                if (data == null)
                    return false;
                Message message = (Message)data;
                if (!message.getOptions().containsKey("~entity.kind"))
                    return false;
                
                Annotation entity = message.getAnnotation("Entity");
                Object cache = entity.getValue("cache");
                
                return cache != null && !Boolean.FALSE.equals(cache) && 
                        Boolean.TRUE.equals(entity.getValue("serial"));
            }
        },
        
        UPDATABLE_SERIAL_CACHE
        {
            public boolean is(Object data)
            {
                if (data == null)
                    return false;
                Message message = (Message)data;
                if (!message.getOptions().containsKey("~entity.kind"))
                    return false;
                
                Integer maxSize = (Integer)message.getOptions().get("~entity.max_size");
                return maxSize.intValue() > 0 ||
                        Boolean.TRUE.equals(message.getOptions().get("~entity.stable_update_size"));
            }
        },
        
        PROTO_PROTOSTUFF_DS_PKG
        {
            public boolean is(Object data)
            {
                return data instanceof Proto && ((Proto)data).getPackageName().startsWith(
                        "com.dyuproject.protostuff.ds.");
            }
        },
        
        STR_ENDS_WITH_UPPER_S
        {
            public boolean is(Object data)
            {
                return data instanceof String && ((String)data).endsWith("S");
            }
        },
        
        STARTS_WITH_DOT
        {
            public boolean is(Object data)
            {
                return data.toString().charAt(0) == '.';
            }
        },
        
        STARTS_WITH_SI
        {
            public boolean is(Object data)
            {
                return data.toString().startsWith("SI_");
            }
        },
        
        SINGLE_COMPUTED_UPDATE_FIELD
        {
            public boolean is(Object data)
            {
                final Object first;
                if (data instanceof Map)
                {
                    if (((Map<?,?>)data).size() != 1)
                        return false;
                    
                    first = ((Map<?,?>)data).values().iterator().next();
                }
                else
                {
                    if (((Collection<?>)data).size() != 1)
                        return false;
                    
                    first = ((Collection<?>)data).iterator().next();
                }
                
                return ((Field<?>)first).getO().containsKey("compute_to_parent");
            }
        },
        
        PARAM_UPDATE
        {
            public boolean is(Object data)
            {
                return ((Message)data).getName().indexOf("Update") != -1;
            }
        },
        
        KEY_OR_ENDS_WITH
        {
            public boolean is(Object data)
            {
                if (data == null)
                    return false;
                
                final String str = data instanceof HasName ? 
                        ((HasName)data).getName() : data.toString();
                
                return str.length() == 3 ? str.equals("key") : str.endsWith("_key");
            }
        },
        
        AV__W
        {
            @Override
            public boolean is(Object data)
            {
                Writable w = (Writable)data;
                Annotation a = w.$val(Annotation.class);
                
                try
                {
                    return a != null && AnnotatedValidation.valueOf(a.getName()) != null;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }
        
        ;
        
        public final IsMap map;

        private Functions()
        {
            map = new IsMap("is_" + name().toLowerCase(), this);
        }
    }
}
