import "fbsgen/base"
import "fbsgen/dict"
import "ds/java/common"

registry_block(registry, module) ::= <<
«registry_body(registry, module, format_to_string.(registry_fill(registry, module, module.w.v.(module.num))), module.w.list)»
>>

registry_body(registry, module, fill, cached) ::= <<
«base_header(false, module, "// ")»

package «module.o.("ds/java/EntityRegistry.java.package_name")»;

import com.dyuproject.protostuffdb.EntityMetadataRegistry;
«if(!is_empty.(cached))»
import com.dyuproject.protostuffdb.EntityCache;
import com.dyuproject.protostuffdb.KVICache;
import com.dyuproject.protostuffdb.SerialArrayCache;
import com.dyuproject.protostuffdb.Visitor;
import com.dyuproject.protostuffdb.WrappedEntityCache;
import com.dyuproject.protostuffdb.WriteContext;
«elseif(module.w.map.("seq"))»
import com.dyuproject.protostuffdb.Visitor;
import com.dyuproject.protostuffdb.WriteContext;
«endif»


/**
 * <pre>
 * Registry for all entities.
 * 
 * max kind: «module.w.val»«if(module.o.("print_max_kind"))»«format_print_out.({max kind: «module.val»})»«endif»
 * </pre>
 */
public final class EntityRegistry
{
    private EntityRegistry() {}

    public static final EntityMetadataRegistry REGISTRY = 
            fill(EntityMetadataRegistry.newBuilder()).build();

    public static EntityMetadataRegistry.Builder fill(EntityMetadataRegistry.Builder builder)
    {
        return builder
                «fill»
                ;
    }
    
    «if(!is_empty.(cached))»
    «cached:{m|«message_cache_block(m, module, m.a.("Entity").p)»}; separator="\n"»
    
    public static void fill(Object[] visitorsByKind)
    {
        «cached:fill_kind(module); separator="\n"»
    }
    
    static void clearCacheForTests()
    {
        «cached:{m|«m.relativeName; format="\\.==&&UUC"»_CACHE.clear();}; separator="\n"»
    }
    
    «endif»
    «init_seq(module.w.map.("seq"), module)»
}

>>

fill_kind(message, module) ::= <%
visitorsByKind[«message.o.("~entity.kind")»] = «message.relativeName; format="\\.==&&UUC"»_CACHE
«if(message.a.("Entity").p.("serial") && is_updatable_serial_cache.(message))»
.visitor
«endif»
;
%>

init_seq(list, module) ::= <<
«if(list)»
private static final com.dyuproject.protostuffdb.VisitorSession.Handler<WriteContext> VSH = 
        new com.dyuproject.protostuffdb.VisitorSession.Handler<WriteContext>()
{
    @Override
    public void handle(com.dyuproject.protostuffdb.VisitorSession session, 
            WriteContext context)
    {
        int kind = 1;
        final byte[] sk = com.dyuproject.protostuffdb.KeyUtil.newEntityRangeKeyStart(kind);
        final byte[] ek = com.dyuproject.protostuffdb.KeyUtil.newEntityRangeKeyEnd(kind);
        
        «list:visit_kind(module); separator="\n\n"»
    }
};

static void initSeq(com.dyuproject.protostuffdb.Datastore store, WriteContext context)
{
    store.session(context, VSH, context);
}

static
{
    if (1 != protostuffdb.Jni.WRITERS)
        throw new RuntimeException(protostuffdb.Jni.SEQ_ERRMSG);
}
«endif»
>>

visit_kind(message, module) ::= <<
kind = «message.relativeName».EM.kind; sk[0] = (byte)kind; ek[0] = (byte)kind;
«if(message.a.("Entity").p.("serial") && is_updatable_serial_cache.(message))»
«message.relativeName; format="\\.==&&UUC"»_CACHE.fillSeqId(«message.relativeName».EM, session, context, sk, ek);
«else»
session.visitRange(true, 1, true, null, Visitor.SET_SEQ_ID, «message.relativeName».EM, sk, 0, sk.length, ek, 0, ek.length);
«endif»
>>

registry_fill(registry, module, w) ::= <<
«registry.entities:entity_block(module); separator="\n"»
>>

// store the kind with the biggest number into val
entity_block(message, module) ::= <%
.add(«message.relativeName».EM) // «message.o.("~entity.kind")»
«if(module.w.n.(message.o.("~entity.kind")).gt.(module.val))»
«module.w.v.(module.num)»
«endif»
«entity_collect(message, module, message.a.("Entity"))»
%>

entity_collect(message, module, a) ::= <%
«if(a.p.("cache"))»
«module.w.add.(message)»
«endif»
«if(a.p.("seq"))»
«module.w.k.("seq").putlist.(message)»
«endif»
%>

message_cache_block(message, module, p) ::= <<
«message_cache_body(message, module, p, p.("cache"), p.("cache_segment_size"), p.("serial"))»
>>

cache_new(message, module, cache_segment_size, serial) ::= <%
new 
«if(serial && is_updatable_serial_cache.(message))»
SerialArrayCache(«message.relativeName».VO_ID, «segment_size(p, cache_segment_size)», «message.o.("~entity.max_size")»)
«else»
KVICache(«message.o.("~entity.kind")», «if(serial)»true«else»1 == protostuffdb.Jni.WRITERS«endif», «segment_size(p, cache_segment_size)»)
«endif»
%>

message_cache_body(message, module, p, cache, cache_segment_size, serial) ::= <<
«if(is_true.(cache))»
public static final «if(serial && is_updatable_serial_cache.(message))»SerialArrayCache«else»KVICache«endif» «message.relativeName; format="\\.==&&UUC"»_CACHE = 
        «cache_new(message, module, cache_segment_size, serial)»;
«elseif(is_instanceof_string.(cache))»
public static final «cache» «message.relativeName; format="\\.==&&UUC"»_CACHE = 
        new «cache»();
«elseif(module.w.k.(cache).ksame.(message))»
public static final EntityCache<«message.relativeName»> «message.relativeName; format="\\.==&&UUC"»_CACHE = 
        new EntityCache<«message.relativeName»>(«message.relativeName».EM, «if(serial)»true«else»false«endif», «segment_size(p, cache_segment_size)»)
{
    public «message.relativeName»[] newArray(int size) { return new «message.relativeName»[size]; }
};
«else»
static final WrappedEntityCache<«cache.relativeName»,«message.relativeName»> «message.relativeName; format="\\.==&&UUC"»_CACHE = 
        new WrappedEntityCache<«cache.relativeName»,«message.relativeName»>(«message.relativeName».EM, «if(serial)»true«else»false«endif», «segment_size(p, cache_segment_size)»)
{
    public «cache.relativeName»[] newArray(int size) { return new «cache.relativeName»[size]; }
    public «message.relativeName» unwrap(«cache.relativeName» wrapper) { return wrapper.«wrapper_field(wrapper=cache, fname=module.w.k.(message.relativeName).fmt.("UC"))»; } 
    public «cache.relativeName» wrap(«message.relativeName» entity) { return new «cache.relativeName»(entity); }
    «if(cache.f.("id"))»
    public void assignIndexTo(«cache.relativeName» item, int index) { unwrap(item).id = item.id = index + 1; }
    «endif»
};
«endif»
>>

segment_size(p, segment_size) ::= <%
«if(segment_size)»
«segment_size»
«else»
512
«endif»
%>

wrapper_field(wrapper, module, fname) ::= <%
«if(wrapper.f.(fname))»
«fname»
«else»
p
«endif»
%>

