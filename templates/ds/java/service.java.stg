import "fbsgen/base"
import "fbsgen/dict"
import "ds/common"
import "ds/service-common"

map_invalid_default ::= [
  "1": " == com.dyuproject.protostuff.ds.P1.DEFAULT_END", 
  "4": " == com.dyuproject.protostuff.ds.P4.DEFAULT_END", 
  "8": " == com.dyuproject.protostuff.ds.P8.DEFAULT_END", 
  "D": " == com.dyuproject.protostuff.ds.PD.DEFAULT_END", 
  "K": " == com.dyuproject.protostuff.ByteString.EMPTY_BYTE_ARRAY", 
  "S": " == com.dyuproject.protostuff.ByteString.EMPTY_STRING"
]

proto_block(proto, module) ::= <<
«proto_block_main(proto, module, module.attrs.("registry"), proto.o.("link"), proto.declaredPackageName, get_op_pkg.(proto), get_view_pkg.(proto))»
>>

proto_block_main(proto, module, registry, link, origPkg, opPkg, viewPkg) ::= <<
«proto_body(proto, module, link, get_config.(link), registry.pkgNestedServicesMap.(origPkg), registry.pkgServicePermutationsMap.(origPkg), new_writable.(""), opPkg, viewPkg)»
>>

proto_body(proto, module, link, config, nsListMap, spListMap, w, opPkg, viewPkg) ::= <<
«base_header(proto, module, "// ")»

package «proto.javaPackageName»;

import java.io.IOException;
import java.io.InputStream;

import com.dyuproject.protostuff.Pipe;
import com.dyuproject.protostuff.RpcCodec;
import com.dyuproject.protostuff.RpcError;
import com.dyuproject.protostuff.RpcHeader;
import com.dyuproject.protostuff.RpcProtocol;
import com.dyuproject.protostuff.RpcResponse;
import com.dyuproject.protostuff.RpcService;
import com.dyuproject.protostuff.RpcWorker;
import com.dyuproject.protostuff.WriteSession;
import com.dyuproject.protostuffdb.Datastore;
import com.dyuproject.protostuffdb.ProtostuffPipe;
import com.dyuproject.protostuffdb.WriteContext;

public final class «link»
{
    private «link»() {}

    «if(nsListMap && spListMap)»
    «link.values:{v|«v_block(v, module, registry, proto, proto.s.(v.name), link, nsListMap.(v.name), spListMap.(v.name), w, config.v.(v.name), opPkg, viewPkg)»}; separator="\n\n"»
    «elseif(spListMap)»
    «link.values:{v|«v_block(v, module, registry, proto, proto.s.(v.name), link, false, spListMap.(v.name), w, config.v.(v.name), opPkg, viewPkg)»}; separator="\n\n"»
    «elseif(nsListMap)»
    «link.values:{v|«v_block(v, module, registry, proto, proto.s.(v.name), link, nsListMap.(v.name), false, w, config.v.(v.name), opPkg, viewPkg)»}; separator="\n\n"»
    «else»
    «link.values:{v|«v_block(v, module, registry, proto, proto.s.(v.name), link, false, false, w, config.v.(v.name), opPkg, viewPkg)»}; separator="\n\n"»
    «endif»
}

>>

v_block(v, module, registry, proto, service, link, nsList, pList, w, cv, opPkg, viewPkg) ::= <<
«if(service)»
«w.addall.(service.rpcs)»
«endif»
«if(nsList)»
«nsList:{s|«w.addall.(s.rpcs)»}»
«endif»
public static abstract class «v.name» extends RpcService
{
    public «v.name»()
    {
        super(«v.number»);
    }

    protected final RpcError handle(RpcHeader header, InputStream in, 
            byte[] inBuffer, int inOffset, int inLimit, RpcCodec inCodec, 
            RpcProtocol protocol, RpcCodec outCodec, 
            RpcWorker worker, WriteSession session, 
            Datastore store, WriteContext context)
            throws IOException
    {
        switch (header.method)
        {
            «module.w.clearnum»
            «w.list:{r|«rpc_switch(r, module, v, module.w.getandinc, opPkg, viewPkg)»}; separator="\n\n"»
            «if(pList)»
            «w.n.(length(w.list))»
            «pList:{p|«p_switch(p, module, v, w.getandinc, {«p_name(p, module)»}, {«p_fqcn(p, module)»}, opPkg, viewPkg, w)»}; separator="\n\n"»
            «w.map.("ac"):{p|«p_switch(p, module, v, w.getandinc, {«p_name(p, module, true)»}, {«p_fqcn(p, module)»}, opPkg, viewPkg, w, true)»}; separator="\n\n"»
            «w.map_remove.("ac")»
            «endif»
            «w.clearList»
            default:
                return outCodec.writeError(RpcError.FAILED, METHOD_NOT_FOUND, 
                        STATUS_MSG_SCHEMA, header, protocol, worker, session);
        }
    }
    
    «if(is_auth_required.(v))»
    «if(v.o.("code_is_authorized"))»
    protected boolean isAuthorized(RpcHeader header)
    {
        «v.o.("code_is_authorized")»
    }
    «elseif(!is_zero.(cv.entrySize))»
    protected boolean isAuthorized(RpcHeader header)
    {
        return 
            «cv.e.values:{e|«e.field.ev.enumGroup.javaFullName».«e.field.ev.name».«cv.config.link.a.("Config").p.("filter_method")»(header)}; separator=" \n&& "»;
    }
    «else»
    protected abstract boolean isAuthorized(RpcHeader header);
    «endif»
    «endif»
    
    «if(service)»
    «service.rpcs:{r|«rpc_abstract(r, module, registry, v, opPkg, viewPkg, get_verb_view.(r))»}; separator="\n"»
    «endif»
    «if(nsList)»
    «nsList:{s|«s.rpcs:{r|«rpc_abstract(r, module, registry, v, opPkg, viewPkg, get_verb_view.(r))»}; separator="\n"»}; separator="\n"»
    «endif»
    «if(pList)»
    «pList:{p|«p_impl(p, module, v, format_to_string.({«p_name(p, module)»}), format_to_string.({«p_fqcn(p, module)»}), opPkg, viewPkg, p.v.o.(module.w.k.("autocomplete_").kappend.(p.name)))»}; separator="\n\n"»
    «endif»
}
>>

ps_val(entity, module, custom_ps, defEntity=false) ::= <%
«if(module.w.v.(custom_ps).val)»
«if(is_true.(module.val))»«module.w.v.(entity)»«entity.javaFullName»«else»«module.val.javaFullName»«endif»
«elseif(module.w.v.(entity.a.("Entity").p.("custom_ps")).val)»
«if(is_true.(module.val))»«module.w.v.(entity)»«entity.javaFullName»«else»«module.val.javaFullName»«endif»
«elseif(!defEntity)»
«entity.javaFullName»
«elseif(is_true.(defEntity))»
«module.w.v.(entity)»«entity.javaFullName»
«else»
«defEntity.javaFullName»
«endif»
%>

// NOTE: stored custom_ps to val
p_switch(p, module, v, idx, name, fqcn, opPkg, viewPkg, w, ac=false) ::= <<
«if(!ac && module.w.k.(p.resolvedIdxString).kew.("S") && p.v.o.(module.w.k.("autocomplete_").kappend.(p.name)))»
«w.k.("ac").putlist.(p)»
«endif»
case «idx»:
{
    «fqcn» req = protocol.parseFrom(inCodec, 
            in, inBuffer, inOffset, inLimit, 
            «fqcn».getSchema(), 
            header, outCodec, worker, session);
    if (req == null)
        return RpcError.INVALID;
    
    «if(ac)»
    final Pipe.Schema<com.dyuproject.protostuff.ds.ACResult.PList> ps =
            com.dyuproject.protostuff.ds.ACResult.PList.getPipeSchema();
    «else»
    final Pipe.Schema<«ps_val(p.v.config.target, module, p.v.config.params.("custom_ps"), true)».PList> ps = 
            «module.val.javaFullName».PList.getPipeSchema();
    «endif»
    
    final RpcResponse res = outCodec.getRpcResponse(
            ps, context, header, protocol, worker, session);
    
    return «name»(req, store, res, ps, header) ? 
            end(res, RpcError.NONE) : end(res, RpcError.FAILED);
}
>>

p_ternary(p, module, pfx, args) ::= <<
«pfx»(
«args»):

«pfx»r(
«args»);
>>

p_ac_init(p, module, ac, ps, message) ::= <<
«if(!ps)»
res.context.$field = «p.lastResolvedEntry.field.number»;
res.context.$offset = «if(p.v.config.target.a.("Entity").p.("cache"))»«message.name».VO_ID«else»0«endif»;
«elseif(is_true.(ps))»
res.context.pipe.init(«message.name».EM, «message.name»Views.ps«message.name»(header), 
        «message.name».M.PList.FN_P, true);
«else»
res.context.pipe.init(«message.name».EM, «ps.relativeName».getPipeSchema(), 
        «ps.relativeName».PList.FN_P, true);
«endif»
>>

// NOTE: stored custom_ps to val
p_impl(p, module, v, name, fqcn, opPkg, viewPkg, ac) ::= <<
public boolean «name»(
        «fqcn» req, 
        Datastore store, RpcResponse res, 
        Pipe.Schema<«ps_val(p.v.config.target, module, p.v.config.params.("custom_ps"))».PList> resPipeSchema, 
        RpcHeader header) throws IOException
{
    «if(module.val)»
    res.context.ps = «get_entity_root_if_nested.(p.v.config.target).name»Views.ps«p.v.config.target.relativeName; format="\\.=="»(header);
    «endif»
    «if(is_size_one.(p.resolvedIdxString))»
    return req.end«map_invalid_default.(p.resolvedIdxString)» ? 
            «p_ternary(p, module, {com.dyuproject.protostuffdb.Visit.by«p.resolvedIdxString»}, p_method_args(p, module, true))»
    «else»
    return req.l.end«map_invalid_default.(module.w.k.(p.resolvedIdxString).kfmt.("LC"))» ? 
            «p_ternary(p, module, {com.dyuproject.protostuff.ds.prk«map_pkg_vx.(module.w.k.(p.resolvedIdxString).kfmt.("FC"))».VisitP«p.resolvedIdxString; format="RM_LC"».byP«p.resolvedIdxString»}, p_method_args(p, module))»
    «endif»

}
«if(ac && module.w.k.(p.resolvedIdxString).kew.("S"))»

public boolean f«name; format="RM_FC"»(
        «fqcn» req, 
        Datastore store, RpcResponse res, 
        Pipe.Schema<com.dyuproject.protostuff.ds.ACResult.PList> resPipeSchema, 
        RpcHeader header) throws IOException
{
    «if(is_instanceof_string.(ac))»
    «elseif(!p.resolvedValue.o.("raw_csv_values"))»
    «p_ac_init(p, module, ac, p.lastResolvedEntry.field.o.("autocomplete_ps"), p.v.config.target)»
    «else»
    «verify_raw_csv_values(p, module, module.w.b.("FN_").b.(module.w.k.(p.lastResolvedEntry.field.name).kfmt.("UUC")).str, p.resolvedValue.o.("raw_csv_values"))»
    «endif»
    
    «if(is_size_one.(p.resolvedIdxString))»
    return com.dyuproject.protostuffdb.Visit.by«p.resolvedIdxString»p(
            «p_method_args(p, module, true, ac)»);
    «else»
    return com.dyuproject.protostuff.ds.prk«map_pkg_vx.(module.w.k.(p.resolvedIdxString).kfmt.("FC"))».VisitP«p.resolvedIdxString; format="RM_LC"».byP«p.resolvedIdxString»p(
            «p_method_args(p, module, false, ac)»);
    «endif»
}
«endif»
>>

verify_raw_csv_values(p, module, pfx, csv) ::= <<
«if(!p.v.config.target.a.("Entity").p.("cache"))»
«if(!module.w.k.(module.w.k.(csv).kfmt.(" ==")).keq.(module.w.b.(pfx).b.(",$KEY").str))»
«error(p.v.eg.proto, {«p.resolvedValue.eg.relativeName»::«p.resolvedValue.name» must have this exact option: raw_csv_values="«pfx», $KEY"})»
«endif»
«elseif(!module.w.k.(module.w.k.(csv).kfmt.(" ==")).keq.(module.w.b.(pfx).b.(",FN_ID").str))»
«error(p.v.eg.proto, {«p.resolvedValue.eg.relativeName»::«p.resolvedValue.name» must have this exact option: raw_csv_values="«pfx», FN_ID"})»
«endif»
>>

rangev_entity_pv(p, module, type) ::= <%
com.dyuproject.protostuffdb.RangeV.«type».
«if(module.val)»
CONTEXT_
«else»
ENTITY_
«endif»
«if(p.resolvedValue.o.("raw_csv_values"))»
«if(module.w.k.(p.resolvedValue.o.("raw_csv_values")).kew.("$KEY"))»
VK_
«else»
MGET_
«endif»
«endif»
PV
%>

common_pv(p, module) ::= <%
«if(!p.resolvedValue.o.("raw_csv_values"))»
AC_WITH_CONTEXT_FIELD
«elseif(p.v.config.target.a.("Entity").p.("cache"))»
AC_WITH_ID
«else»
AC_WITH_KEY
«endif»
%>

p_pv(p, module, csv) ::= <%
«if(!csv)»«elseif(module.w.k.(csv).kew.("$KEY"))»
VK_
«else»
«error(p.v.eg.proto, {«p.resolvedValue.eg.relativeName»::«p.resolvedValue.name»::raw_csv_values must end with $KEY.})»
«endif»
PV
%>

p_ac_args(p, module, ac, ps) ::= <<
«if(!ps)»
com.dyuproject.protostuff.ds.ACResult.PList.FN_P, 
com.dyuproject.protostuffdb.RangeV.Store.«p_pv(p, module, p.resolvedValue.o.("raw_csv_values"))», store, res.context, 
com.dyuproject.protostuffdb.CommonPV.BUILTIN.«common_pv(p, module)», res
«else»
«if(is_true.(ps))»«p.v.config.target.name».M«else»«ps.relativeName»«endif».PList.FN_P, 
com.dyuproject.protostuffdb.RangeV.Store.«p_pv(p, module, p.resolvedValue.o.("raw_csv_values"))», store, res.context, 
RpcResponse.PIPED_VISITOR, res
«endif»
>>

p_method_args(p, module, oneChar=false, ac=false) ::= <<
«p_idx_fqcn(p, module, oneChar, ac)», 
req, 
«p.v.config.target.javaFullName».EM, 
«if(ac)»
«p_ac_args(p, module, ac, p.lastResolvedEntry.field.o.("autocomplete_ps"))»
«else»
«p.v.config.target.javaFullName».PList.FN_P, 
«rangev_entity_pv(p, module, "Store")», store, res.context, 
com.dyuproject.protostuffdb.RangeV.RES_PV, res
«endif»
>>

p_idx_fqcn(p, module, oneChar, ac) ::= <<
«if(is_instanceof_string.(ac))»
«p.v.config.target.javaFullName».«ac»
«elseif(p.v.parentKey)»
req«if(!oneChar)».l«endif».prk.parentKey == null ? 
    «p.v.config.target.javaFullName».«em_idx_name(p.resolvedValue, module)»: 
    «p.v.config.target.javaFullName».IDX_«p.v.parentKey.name; format="UPPER"»__«p.resolvedValue.name»
«else»
«p.v.config.target.javaFullName».«em_idx_name(p.resolvedValue, module)»
«endif»
>>

rpc_switch(rpc, module, v, idx, opPkg, viewPkg) ::= <<
case «idx»:
{
    «if(rpc.voidBoth)»
    «rpc_switch_void_both(rpc, module, v, opPkg, viewPkg)»
    «elseif(rpc.voidArgType)»
    «rpc_switch_void_request(rpc, module, v, opPkg, viewPkg, rpc.javaReturnType)»
    «elseif(rpc.voidReturnType)»
    «rpc_switch_void_response(rpc, module, v, opPkg, viewPkg, rpc.javaArgType)»
    «else»
    «rpc_switch_void_none(rpc, module, v, opPkg, viewPkg, rpc.javaArgType, rpc.javaReturnType)»
    «endif»
}
>>

rpc_switch_void_both(rpc, module, v, opPkg, viewPkg) ::= <<
String errMsg = «rpc.name»(store, context, header);
if (errMsg == null)
{
    protocol.writeHeaderZeroContent(worker, session);
    return RpcError.NONE;
}

return outCodec.writeError(RpcError.FAILED, errMsg, STATUS_MSG_SCHEMA, 
        header, protocol, worker, session);
>>

rpc_switch_void_none(rpc, module, v, opPkg, viewPkg, javaArgType, javaReturnType) ::= <<
«javaArgType» req = protocol.parseFrom(inCodec, 
        in, inBuffer, inOffset, inLimit, 
        «javaArgType».getSchema(), 
        header, outCodec, worker, session);
if (req == null)
    return RpcError.INVALID;

final RpcResponse res = outCodec.getRpcResponse(
        «javaReturnType».getPipeSchema(), 
        context, header, protocol, worker, session);

return «rpc.name»(req, store, res, 
        «javaReturnType».getPipeSchema(), header) ? 
                end(res, RpcError.NONE) : end(res, RpcError.FAILED);

>>

rpc_switch_void_request(rpc, module, v, opPkg, viewPkg, javaReturnType) ::= <<
final RpcResponse res = outCodec.getRpcResponse(
        «javaReturnType».getPipeSchema(), 
        context, header, protocol, worker, session);

return «rpc.name»(store, res, 
        «javaReturnType».getPipeSchema(), header) ? 
                end(res, RpcError.NONE) : end(res, RpcError.FAILED);
>>

rpc_switch_void_response(rpc, module, v, opPkg, viewPkg, javaArgType) ::= <<
«javaArgType» req = protocol.parseFrom(inCodec, 
        in, inBuffer, inOffset, inLimit, 
        «javaArgType».getSchema(), 
        header, outCodec, worker, session);
if (req == null)
    return RpcError.INVALID;

String errMsg = «rpc.name»(req, store, context, header);
if (errMsg == null)
{
    protocol.writeHeaderZeroContent(worker, session);
    return RpcError.NONE;
}

return outCodec.writeError(RpcError.FAILED, errMsg, STATUS_MSG_SCHEMA, 
        header, protocol, worker, session);
>>

rpc_abstract(rpc, module, registry, v, opPkg, viewPkg, viewVerb) ::= <<

«if(rpc.voidBoth)»
«if(viewVerb)»
«rpc_view_void_both(rpc, module, v, opPkg, viewPkg, registry.lookupEntityMap.({«verb_entity_name(viewVerb, rpc)»}))»
«else»
«rpc_op_void_both(rpc, module, v, opPkg, viewPkg, registry.lookupEntityMap.({«verb_entity_name(get_verb_op.(rpc), rpc)»}))»
«endif»
«elseif(rpc.voidArgType)»
«if(viewVerb)»
«rpc_view_void_request(rpc, module, v, opPkg, viewPkg, registry.lookupEntityMap.({«verb_entity_name(viewVerb, rpc)»}))»
«else»
«rpc_op_void_request(rpc, module, v, opPkg, viewPkg, registry.lookupEntityMap.({«verb_entity_name(get_verb_op.(rpc), rpc)»}))»
«endif»
«elseif(rpc.voidReturnType)»
«if(viewVerb)»
«rpc_view_void_response(rpc, module, v, opPkg, viewPkg, registry.lookupEntityMap.({«verb_entity_name(viewVerb, rpc)»}))»
«else»
«rpc_op_void_response(rpc, module, v, opPkg, viewPkg, registry.lookupEntityMap.({«verb_entity_name(get_verb_op.(rpc), rpc)»}))»
«endif»
«elseif(viewVerb)»
«rpc_view_void_none(rpc, module, v, opPkg, viewPkg, get_entity_rpc_return_type.(rpc), registry.lookupEntityMap.({«verb_entity_name(viewVerb, rpc)»}))»
«else»
«rpc_op_void_none(rpc, module, v, opPkg, viewPkg, get_entity_rpc_return_type.(rpc), registry.lookupEntityMap.({«verb_entity_name(get_verb_op.(rpc), rpc)»}))»
«endif»

>>

/* ================================================== */

rpc_view_void_none(rpc, module, v, opPkg, viewPkg, retEntity, verbEntity) ::= <<
«if(rpc.service.nested)»
«rpc_impl_void_none(rpc, module, v, opPkg, viewPkg, rpc.service.parentMessage, rpc.javaArgType, rpc.javaReturnType, true)»
«elseif(retEntity)»
«rpc_impl_void_none(rpc, module, v, opPkg, viewPkg, retEntity, rpc.javaArgType, rpc.javaReturnType, true)»
«elseif(is_entity.(verbEntity))»
«rpc_impl_void_none(rpc, module, v, opPkg, viewPkg, verbEntity, rpc.javaArgType, rpc.javaReturnType, true)»
«else»
«rpc_abstract_void_none(rpc, module, v, opPkg, viewPkg)»
«endif»
>>

rpc_op_void_none(rpc, module, v, opPkg, viewPkg, retEntity, verbEntity) ::= <<
«if(rpc.service.nested)»
«rpc_impl_void_none(rpc, module, v, opPkg, viewPkg, rpc.service.parentMessage, rpc.javaArgType, rpc.javaReturnType)»
«elseif(retEntity)»
«rpc_impl_void_none(rpc, module, v, opPkg, viewPkg, retEntity, rpc.javaArgType, rpc.javaReturnType)»
«elseif(is_entity.(verbEntity))»
«rpc_impl_void_none(rpc, module, v, opPkg, viewPkg, verbEntity, rpc.javaArgType, rpc.javaReturnType)»
«elseif(is_entity.(rpc.argType.parentMessage))»
«rpc_impl_void_none(rpc, module, v, opPkg, viewPkg, rpc.argType.parentMessage, rpc.javaArgType, rpc.javaReturnType)»
«else»
«rpc_abstract_void_none(rpc, module, v, opPkg, viewPkg)»
«endif»
>>

// NOTE: stored custom_ps to val
pipe_schema_var(rpc, module, entity, retParent, listv, var) ::= <<
final Pipe.Schema<«ps_val(entity, module, listv.p.("custom_ps"), retParent)»> «var» = 
        «if(!module.val)»
        «retParent.relativeName».getPipeSchema();
        «elseif(!module.w.k.(retParent.name).keq.("M"))»
        «get_entity_root_if_nested.(entity).name»Views.ps«retParent.relativeName; format="\\.=="»(header);
        «else»
        «get_entity_root_if_nested.(entity).name»Views.ps«entity.relativeName; format="\\.=="»(header);
        «endif»
>>

rpc_impl_void_none(rpc, module, v, opPkg, viewPkg, entity, javaArgType, javaReturnType, readOnly=false) ::= <<
«if(readOnly && rpc.a.("ListV"))»
public boolean «rpc.name»(
        «javaArgType» req, 
        Datastore store, 
        RpcResponse res, Pipe.Schema<«javaReturnType»> resPipeSchema, 
        RpcHeader header) throws IOException
{
    «pipe_schema_var(rpc, module, entity, rpc.returnType.parentMessage, rpc.a.("ListV"), "ps")»
    «if(is_entity.(rpc.a.("ListV").p.("entry")))»
    final ProtostuffPipe pipe = res.context.pipe.init(
            «entity.relativeName».EM, ps, «javaReturnType».FN_P, true);
    try
    {
        store.visitKind(«rpc.a.("ListV").p.("entry").relativeName».EM, 
                -1, «if(rpc.a.("ListV").p.("desc"))»true«else»false«endif», null, req.key, 
                res.context, RpcResponse.PIPED_VISITOR, res);
    }
    finally
    {
        pipe.clear();
    }
    return true;
    «else»
    return com.dyuproject.protostuffdb.ListV.list(
            «entity.relativeName».EM, «javaReturnType».FN_P, 
            req, store, res, ps);
    «endif»
}
«else»
/**
 * Mapped to {@link «static_method_name(entity, module, rpc, "#", readOnly, opPkg, viewPkg, rpc.a.("X"))»}.
 */
public boolean «rpc.name»(
        «javaArgType» req, 
        Datastore store, 
        RpcResponse res, Pipe.Schema<«javaReturnType»> resPipeSchema, 
        RpcHeader header) throws IOException
{
    return «static_method_name(entity, module, rpc, ".", readOnly, opPkg, viewPkg, rpc.a.("X"))»(
            req, store, res, resPipeSchema, header);
}
«endif»
>>

rpc_abstract_void_none(rpc, module, v, opPkg, viewPkg) ::= <<
public abstract boolean «rpc.name»(
        «rpc.javaArgType» req, 
        Datastore store, 
        RpcResponse res, Pipe.Schema<«rpc.javaReturnType»> resPipeSchema, 
        RpcHeader header) throws IOException;
>>

/* ================================================== */

rpc_view_void_response(rpc, module, v, opPkg, viewPkg, verbEntity) ::= <<
«if(rpc.service.nested)»
«rpc_impl_void_response(rpc, module, v, opPkg, viewPkg, rpc.service.parentMessage, true)»
«elseif(is_entity.(verbEntity))»
«rpc_impl_void_response(rpc, module, v, opPkg, viewPkg, verbEntity, true)»
«else»
«rpc_abstract_void_response(rpc, module, v, opPkg, viewPkg)»
«endif»
>>

rpc_op_void_response(rpc, module, v, opPkg, viewPkg, verbEntity) ::= <<
«if(rpc.service.nested)»
«rpc_impl_void_response(rpc, module, v, opPkg, viewPkg, rpc.service.parentMessage)»
«elseif(module.w.k.("target").kin.(rpc.a.("X")))»
«rpc_impl_void_response(rpc, module, v, opPkg, viewPkg, rpc.a.("X").p.("target"))»
«elseif(rpc.a.("X"))»
«rpc_abstract_void_response(rpc, module, v, opPkg, viewPkg)»
«elseif(is_entity.(verbEntity))»
«rpc_impl_void_response(rpc, module, v, opPkg, viewPkg, verbEntity)»
«elseif(is_entity.(rpc.argType.parentMessage))»
«rpc_impl_void_response(rpc, module, v, opPkg, viewPkg, rpc.argType.parentMessage)»
«else»
«rpc_abstract_void_response(rpc, module, v, opPkg, viewPkg)»
«endif»
>>

rpc_impl_void_response(rpc, module, v, opPkg, viewPkg, entity, readOnly=false) ::= <<
/**
 * Mapped to {@link «static_method_name(entity, module, rpc, "#", readOnly, opPkg, viewPkg, rpc.a.("X"))»}.
 */
public String «rpc.name»(«rpc.javaArgType» req, 
        Datastore store, WriteContext context, 
        RpcHeader header) throws IOException
{
    return «static_method_name(entity, module, rpc, ".", readOnly, opPkg, viewPkg, rpc.a.("X"))»(
            req, store, context, header);
}
>>

rpc_abstract_void_response(rpc, module, v, opPkg, viewPkg) ::= <<
public abstract String «rpc.name»(«rpc.javaArgType» req, 
        Datastore store, WriteContext context, 
        RpcHeader header) throws IOException;
>>

/* ================================================== */

rpc_view_void_request(rpc, module, v, opPkg, viewPkg, verbEntity) ::= <<
«if(rpc.service.nested)»
«rpc_impl_void_request(rpc, module, v, opPkg, viewPkg, rpc.service.parentMessage, rpc.javaReturnType, true)»
«elseif(is_entity.(verbEntity))»
«rpc_impl_void_request(rpc, module, v, opPkg, viewPkg, verbEntity, rpc.javaReturnType, true)»
«else»
«rpc_abstract_void_request(rpc, module, v, opPkg, viewPkg)»
«endif»
>>

rpc_op_void_request(rpc, module, v, opPkg, viewPkg, verbEntity) ::= <<
«if(rpc.service.nested)»
«rpc_impl_void_request(rpc, module, v, opPkg, viewPkg, rpc.service.parentMessage, rpc.javaReturnType)»
«elseif(module.w.k.("target").kin.(rpc.a.("X")))»
«rpc_impl_void_request(rpc, module, v, opPkg, viewPkg, rpc.a.("X").p.("target"), rpc.javaReturnType)»
«elseif(!rpc.a.("X") && is_entity.(verbEntity))»
«rpc_impl_void_request(rpc, module, v, opPkg, viewPkg, verbEntity, rpc.javaReturnType)»
«else»
«rpc_abstract_void_request(rpc, module, v, opPkg, viewPkg)»
«endif»
>>

rpc_impl_void_request(rpc, module, v, opPkg, viewPkg, entity, javaReturnType, readOnly=false) ::= <<
«if(readOnly && rpc.a.("ListV"))»
public boolean «rpc.name»(
        Datastore store, 
        RpcResponse res, 
        Pipe.Schema<«javaReturnType»> resPipeSchema, 
        RpcHeader header) throws IOException
{
    «pipe_schema_var(rpc, module, entity, rpc.returnType.parentMessage, rpc.a.("ListV"), "ps")»
    «if(!entity.a.("Entity").p.("cache"))»
    // eager fetch?
    return com.dyuproject.protostuffdb.ListV.list(
            «entity.relativeName».EM, «javaReturnType».FN_P, «module.w.v.(rpc.a.("ListV").p.("limit")).velse.("-1")», 
            false, null, null, store, res, ps);
    «elseif(module.val || is_entity.(rpc.returnType.parentMessage))»
    final ProtostuffPipe pipe = res.context.pipe.init(
            «entity.relativeName».EM, ps, «javaReturnType».FN_P, true);
    try
    {
        EntityRegistry.«entity.relativeName; format="\\.==&&UUC"»_CACHE.pipeTo(res);
    }
    finally
    {
        pipe.clear();
    }
    return true;
    «else»
    EntityRegistry.«entity.relativeName; format="\\.==&&UUC"»_CACHE.writeTo(res.output, 
            ps.wrappedSchema, «javaReturnType».FN_P);
    return true;
    «endif»
}
«else»
/**
 * Mapped to {@link «static_method_name(entity, module, rpc, "#", readOnly, opPkg, viewPkg, rpc.a.("X"))»}.
 */
public boolean «rpc.name»(
        Datastore store, 
        RpcResponse res, 
        Pipe.Schema<«javaReturnType»> resPipeSchema, 
        RpcHeader header) throws IOException
{
    return «static_method_name(entity, module, rpc, ".", readOnly, opPkg, viewPkg, rpc.a.("X"))»(
            store, res, resPipeSchema, header);
}
«endif»
>>

rpc_abstract_void_request(rpc, module, v, opPkg, viewPkg) ::= <<
public abstract boolean «rpc.name»(
        Datastore store, 
        RpcResponse res, 
        Pipe.Schema<«rpc.javaReturnType»> resPipeSchema, 
        RpcHeader header) throws IOException;
>>

/* ================================================== */

rpc_view_void_both(rpc, module, v, opPkg, viewPkg, verbEntity) ::= <<
«if(rpc.service.nested)»
«rpc_impl_void_both(rpc, module, v, opPkg, viewPkg, rpc.service.parentMessage, true)»
«elseif(is_entity.(verbEntity))»
«rpc_impl_void_both(rpc, module, v, opPkg, viewPkg, verbEntity, true)»
«else»
«rpc_abstract_void_both(rpc, module, v, opPkg, viewPkg)»
«endif»
>>

rpc_op_void_both(rpc, module, v, opPkg, viewPkg, verbEntity) ::= <<
«if(rpc.service.nested)»
«rpc_impl_void_both(rpc, module, v, opPkg, viewPkg, rpc.service.parentMessage)»
«elseif(module.w.k.("target").kin.(rpc.a.("X")))»
«rpc_impl_void_both(rpc, module, v, opPkg, viewPkg, rpc.a.("X").p.("target"))»
«elseif(!rpc.a.("X") && is_entity.(verbEntity))»
«rpc_impl_void_both(rpc, module, v, opPkg, viewPkg, verbEntity)»
«else»
«rpc_abstract_void_both(rpc, module, v, opPkg, viewPkg)»
«endif»
>>

rpc_impl_void_both(rpc, module, v, opPkg, viewPkg, entity, readOnly=false) ::= <<
/**
 * Mapped to {@link «static_method_name(entity, module, rpc, "#", readOnly, opPkg, viewPkg, rpc.a.("X"))»}.
 */
public String «rpc.name»(Datastore store, WriteContext context, 
        RpcHeader header) throws IOException
{
    return «static_method_name(entity, module, rpc, ".", readOnly, opPkg, viewPkg, rpc.a.("X"))»(
            store, context, header);
}
>>

rpc_abstract_void_both(rpc, module, v, opPkg, viewPkg) ::= <<
public abstract String «rpc.name»(Datastore store, WriteContext context, 
        RpcHeader header) throws IOException;
>>

/* ================================================== */

static_method_name(entity, module, rpc, sep, readOnly, opPkg, viewPkg, x) ::= <%
«if(readOnly)»
«viewPkg».«get_entity_root_if_nested.(entity).name»Views«sep»«rpc.name»
«else»
«opPkg».«if(x)»X«endif»«get_entity_root_if_nested.(entity).name»Ops«sep»«x_op_name(x, rpc)»
«endif»
%>

// TODO x && x.p.("op")
x_op_name(x, rpc) ::= <%
«if(!x)»
«rpc.name»
«elseif(x.p.("op"))»
«x.p.("op")»
«else»
«rpc.name»
«endif»
%>
