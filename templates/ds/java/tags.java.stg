import "ds/common"
import "ds/java/common"

enum_block(eg, module) ::= <<
«base_header(eg.proto, module, "// ")»

package «eg.proto.javaPackageName»;

«enum_javadoc(eg, module)»
public final class «eg.name»
{
    private «eg.name»() {}
    
    «eg.uniqueSortedDeclaredValues:v_declare(module); separator="\n"»
    
    «static_is_user_managed(eg, module, filter_ev_ann_user_managed.(eg))»
    
    «static_get_name(eg, module)»
}
>>

static_is_user_managed(eg, module, values) ::= <<
/**
 * Returns true if the tag is managed by the user.
 * This is configured with a "@UserManaged" annotation in the .proto files.
 * For the tags not configured, it is assumed that the tag is referenced 
 * somewhere in an entity index, which in turn allows it to be managed by 
 * the datastore (tag index inserts/updates/deletes).
 */
public static boolean isUserManaged(int tag)
{
    «if(values.empty)»
    return false;
    «else»
    switch(tag)
    {
        «values:{ v | case «v.name»: return true;}; separator="\n"»
        default: return false;
    }
    «endif»
}
>>

static_get_name(eg, module) ::= <<
public static String getName(int tag)
{
    switch(tag)
    {
        «eg.uniqueSortedDeclaredValues:{ v | case «v.name»: return "«v.name»";}; separator="\n"»
        default: return null;
    }
}
>>

v_declare(v, module) ::= <<
«verify_tag.(v)»
«if(!v.comments.empty)»
/**
«v.comments:{c|  * «c»}; separator="\n"»
 */
«endif»
public static final int «v.name» = «v.number»;
>>
