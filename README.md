# compiler for .proto files with a custom DSL

### Setup
```sh
git clone https://gitlab.com/dyu/ds-proto.git p
```

### Build
```sh
mvn install
```
