#!/bin/sh

CURRENT_DIR=$PWD
# locate
if [ -z "$BASH_SOURCE" ]; then
    SCRIPT_DIR=`dirname "$(readlink -f $0)"`
elif [ -e '/bin/zsh' ]; then
    F=`/bin/zsh -c "print -lr -- $BASH_SOURCE(:A)"`
    SCRIPT_DIR=`dirname $F`
elif [ -e '/usr/bin/realpath' ]; then
    F=`/usr/bin/realpath $BASH_SOURCE`
    SCRIPT_DIR=`dirname $F`
else
    F=$BASH_SOURCE
    while [ -h "$F" ]; do F="$(readlink $F)"; done
    SCRIPT_DIR=`dirname $F`
fi

# ==================================================

PROPS_FILE=_.properties
[ ! -e $CURRENT_DIR/$PROPS_FILE ] && echo "$PROPS_FILE not found on current dir." && exit 0

JAR_FILE=$SCRIPT_DIR/fbsgen-ds-core/target/fbsgen-ds.jar
#[ ! -e $JAR_FILE ] && [ -e $SCRIPT_DIR/templates/fbsgen.jar ] && JAR_FILE=$SCRIPT_DIR/templates/fbsgen.jar

if [ ! -n "$TEMPLATE_PATH" ]; then
    TEMPLATE_PATH=.,$SCRIPT_DIR/templates
    [ -d $CURRENT_DIR/templates ] && TEMPLATE_PATH=templates,$SCRIPT_DIR/templates
fi

if [ ! -n "$PROTO_PATH" ]; then
    PARENT_DIR=`dirname "$CURRENT_DIR"`
    BASE_PARENT=`basename "$PARENT_DIR"`
    if [ "$BASE_PARENT" = 'modules' ]; then
        PROTO_PATH=..
    elif [ -d p ]; then
        PROTO_PATH=p
    elif [ -d ../proto/base ]; then
        # legacy setup
        PROTO_PATH=../proto/base,proto/shared,proto/server
    elif [ -d proto ]; then
        PROTO_PATH=proto
    else
        PROTO_PATH=.
    fi
fi

# proto search strategy
# setting it to 4 will search in this order: proto_path, relative_path, classpath
[ -n "$PROTO_SS" ] || PROTO_SS=4

# The search strategy for templates is in this order: template_path, classpath 
java $JAVA_PROPS -Dtemplate_path=$TEMPLATE_PATH \
    -Dproto_path=$PROTO_PATH \
    -Dproto_search_strategy=$PROTO_SS \
    -Dfbsgen.print_stack_trace=false \
    -Dfbsgen.sequential_field_numbers=true \
    -Dfbsgen.enum_allow_negative=false \
    -jar $JAR_FILE $PROPS_FILE $@
