## 1.2.2 (2023-09-13)
* bump fbsgen dependency to 1.1.2

## 1.2.1 (2023-09-06)
* bump fbsgen dependency to 1.1.1

## 1.2.0 (2023-06-07)
* bump fbsgen dependency to 1.1.0

## 1.1.1 (2023-05-04)
* update dart codegen enabling null-safety

## 1.1.0 (2023-04-29)
* enum mapping handling

## 1.0.18 (2020-03-20)

* add singular fields filter

## 1.0.17 (2017-02-22)

* update fbsgen/cpp2 template to be output compatible with fbsgen/cpp

## 1.0.16 (2017-02-21)

* fix missing `json-w` resource

## 1.0.15 (2017-02-21)

* add `fbs_schema.h` generator

## 1.0.14 (2017-12-29)

* fix @Unique validation on string fields

## 1.0.13 (2017-12-05)

* SyncPub annotation
* fix qform suggest field

## 1.0.12 (2017-10-04)

* add generator templates/ds/json-w

## 1.0.11 (2017-10-03)

* validate kind of parent entity

## 1.0.10 (2017-09-01)

* update from suggest/autocomplete api changes

## 1.0.9 (2017-08-03)

* Added helper method: ```is_service_exclude_client```

## 1.0.8 (2017-06-23)

* ignore ```@Pub``` annotation if the return type is void

## 1.0.7 (2017-06-19)

* rpc flags
* ```@Pub``` annotation on rpc to publish data updates/changes

## 1.0.6 (2017-06-14)

* use 'access_token' instead of 't' when the option 'oauth' is provided

## 1.0.5

* verification of custom defined raw_csv_values on secondary indexes
* use store multiplier on ts qform

## 1.0.4

* add Entity flags
* add sequence keys support

## 1.0.3

* use KeyLockFactory for the entity ops

## 1.0.2

* support ```@ReadOnly``` annotation on rpc methods.
* dart rpc codegen for query and suggest/autocomplete

## 1.0.1

* standalone fatjar that includes all the templates

## 1.0.0

Codegen for:
* cpp - flatbuffers
* java - protostuff with validation, secondary index, rpc stubs and impl
* typescript - message from/to json with rpc, validation
* dart - message from/to json with rpc
* json - service/rpc descriptor
* nginx - service/rpc conf to a backend with redis protocol (getrange/setrange commands)


